FROM rocker/r-ubuntu:20.04

LABEL org.opencontainers.image.authors="Aurelien Ginolhac <aurelien.ginolhac@uni.lu>"

LABEL description="Docker image to build the R teaching websites"

RUN  apt-get update && \
  apt-get install -y --allow-downgrades --fix-missing \
  --allow-remove-essential --allow-change-held-packages \
  --allow-unauthenticated --no-install-recommends --no-upgrade \
  curl netbase \
  zip git cmake \
  libcurl4-openssl-dev \
  libxml2-dev \
  libssl-dev \
  gpg-agent gnupg \
  libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev \
  libfontconfig1-dev \
  libcairo2-dev libmagick++-dev libpoppler-cpp-dev \
  pandoc \
  libharfbuzz-dev libfribidi-dev \
  && rm -rf /var/lib/apt/lists/*

# from https://hub.docker.com/r/justinribeiro/chrome-headless/dockerfile
RUN curl -LO https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    apt-get update -qq && apt-get -y install \
    ./google-chrome-stable_current_amd64.deb && rm google-chrome-stable_current_amd64.deb
# from https://quarto.org/docs/get-started/
# runner are blocked to download from GitHub, so auto hosting the deb
ARG QUARTO_VERSION="1.3.21"
RUN curl -LO http://biostat2.uni.lu/quarto-${QUARTO_VERSION}-linux-amd64.deb && \
    apt-get update -qq && apt-get -y install \
    ./quarto-${QUARTO_VERSION}-linux-amd64.deb && rm quarto-${QUARTO_VERSION}-linux-amd64.deb

# same problem with TinyTex, quarto install, tinytex::install fail on gitlab runners
#RUN curl -LO http://biostat2.uni.lu/TinyTeX-1-v2022.09.tar.gz && \
#    tar xzf TinyTeX-1-v2022.09.tar.gz -C /root && rm TinyTeX-1-v2022.09.tar.gz && \
#    cd /root/.TinyTeX/bin/x86_64-linux && ./tlmgr option sys_bin /usr/local/bin && \
#    ./tlmgr postaction install script xetex  && \
#    ./tlmgr option repository ctan && ./tlmgr path add

RUN install.r renv
