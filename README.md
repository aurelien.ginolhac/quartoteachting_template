
# Quarto template for conversion from xaringan


### Example half done 

- `xaringan`: [source](https://gitlab.lcsb.uni.lu/r-training/rworkshop/-/blob/main/site/lectures/intro.Rmd) and [rendered](https://rworkshop.uni.lu/lectures/intro.html#1)

- `Quarto`: [source](https://gitlab.lcsb.uni.lu/aurelien.ginolhac/quartoteachting_template/-/blob/main/intro.qmd) and [rendered](http://aurelien.ginolhac.pages.uni.lu/quartoteachting_template/intro.html)

Of note, this website works ONLY inside the UNI network and does not have a proper SSL certificate, accept the exception.

### Conversion of old style chunks options to hash pipes

`knitr` has a parser available, here for a filename to be overwritten:

``` r
knitr::convert_chunk_header(input = "filename.Rmd", output = identity)
```

### RMarkdown rendering function

rmarkdown::render()

### RStudio snippets

My **snippets** for markdown documents are:

```
snippet r-logo
	{{< fa brands r-project >}}

snippet qexit
	::: {.content-hidden when-format="html"}
	${0}
	:::

snippet frag
	::: {.fragment}
	${0}
	:::

snippet absolute
	![](${0}){.absolute height="150px" top="1em" right="1em"}

snippet qcol
	:::: {.columns}
	::: {.column width="50%"}
	${1}
	:::
	::: {.column width="50%"}
	${2}
	:::
	::::

snippet ref
	^[**${0}** _et al._ 2015. [_${1}_](${2})]

snippet attr
	::: {.attribution}
	${0}
	:::

snippet q2col
	::: {layout-ncol=2}
	${0}
	:::

snippet center
	::: {.center}
	${0}
	:::

snippet footer
	::: {.footer}
	${0}
	:::
	
snippet qtip
	::: {.callout-tip}
	## Tip
	${0}
	:::

snippet qwarn
	::: {.callout-warning}
	## Warning
	${0}
	:::


snippet qnote
	::: {.callout-note}
	## Note
	${0}
	:::
	
snippet qdanger
	::: {.callout-important}
	## Danger
	${0}
	:::
	
snippet qcaution
	::: {.callout-caution}
	## Danger
	${0}
	:::
```

### Replacement work in progress code

Replace .flex boxes

str_replace(".flex[[[title  long]]]", "\\.flex\\[+([[:alnum:][:blank:][:punct:]^[\\[\\]]]+)\\]+", "\\1")

### Replacement - Working



When you type one word, like `qtip` press SHIFT + Tab and the snippet gets inserted.



### Multiline matching examples

Note: linebreaks don't work as expected
```
".pull-right[ ### Header \\n Text and stuff ]\\n\\n##"

str_replace(slide, "\\.pull-right\\[ (.+).\\].+##", '::: {.columns width = "45%"}\\n\\1\\n:::\\n::::\\n\\n##')
```

