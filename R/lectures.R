library(chromote)
suppressPackageStartupMessages(library(pdftools))
library(renderthis) # jhelvy/renderthis
library(ragg)

slides_knit <- function(Rmd) {
  if (is.na(Rmd) | !file.exists(Rmd)) rlang::abort("Rmd file doesn't exist")

  # to avoid crayon in the slides that destroys css
  withr::local_options(list(crayon.enabled = NULL))
  slide <- rmarkdown::render(input = Rmd,
                             output_format = "xaringan::moon_reader",
                             quiet = TRUE)
  fs::path("lectures", fs::path_file(slide))
}

html_to_pdf <- function(html) {
  if (!file.exists(html)) stop("html file don't exist", call. = FALSE)
  to_pdf(html, partial_slides = TRUE)

}