# Xaringan to reveal

library(tidyverse)

update_header <- function(rmd_file){
    fm <- rmarkdown::yaml_front_matter(rmd_file)
  
paste0('---
title: "', fm$title, '"
subtitle: "', fm$params$subtitle, '"
date: "2023-01-01"
date-format: "dddd MMMM YYYY"
institute: "University of Luxembourg"
author: "', fm$params$author,'"
format:
  revealjs:
    footnotes-hover: true
from: markdown+emoji
execute: 
  echo: false
---

## Title

:::: {.columns}
'  )
#format
#unilu-theme-revealjs:
#  citations-hover: true
# revealjs-plugins:
#   - pointer
# - attribution



}  


update_body <- function(rmd_file) {
  read_file(rmd_file) |>
    # Removes the YAML header
    str_remove_all(regex("---(\\n|.)+?---", multiline = TRUE)) |>
    
    # TODO remove setup lectures
    str_remove(regex("```\\{r setup-lecture(.)+?\\r\\n```")) |>  
    
    #Remove --- for slides and --
    str_remove_all("\\n---") |> 
    str_remove_all("\\n--") |> 
    
    #Replace Fontawesome
    str_replace_all(regex("\\{+\\< fa brands r-project >\\}+"), '`r fontawesome::fa("r-project")`' ) |> 
    # TODO replace/setup title%----- slides
     # What we thought of as a solution. However we run into some problems with the quotations. Needs refining but "works"
    
    # replaces [rgb]box with heading 3  -  This was interfering with the code below. 
    #str_replace_all(".*[rgb]box\\[(.*)", "### \\1") |>
    
    # TODO replaces any flex box and w-x0 with heading or callout
    # Tested in document and works!
    str_replace_all(regex("(?s)\\.flex.*?\\[.*?\\[.*?\\[" , multiline = T), "\\\n:::{.callout-important}\\\n##") |>
    str_replace_all(regex("(?s)\\.w.*?\\[.*?\\[.*?\\[" , multiline = T), "\\\n:::{.callout-note}\\\n##") |>  
    
    #replacing htmltools
    
    #top right parameter
    str_replace_all(regex('`{3}\\{r, echo = FALSE\\}\\r\\nhtmltools:{2}.*?"(.*)",\\\r\\n               style = "height:(.*);position: .*?;top: (.*);right: (.*);".\\r\\n```'), 'replaced image\\\n![](\\1){.absolute height="\\2" top="1em" right="\\3"}') |> 
    
    #bottom left parameter
    str_replace_all(regex('`{3}\\{r, echo = FALSE\\}\\r\\nhtmltools:{2}.*?"(.*)",\\\r\\n               style = "height:(.*);position: .*?;bottom: (.*);left: (.*);".\\r\\n```'), 'replaced image\\\n![](\\1){.absolute height="\\2" bottom="1em" left="\\3"}') |>
    
    #Also for w-50 - This didn't work.
    #str_replace_all(regex("(?s)^\\.flex.*?50\\..*?\\[" , multiline = T), "50\\\n:::\\\n##") |>
    #Also for w-30 - This didn't work.
    #str_replace_all(regex("(?s)^\\.flex.*?50\\..*?\\[" , multiline = T), "30\\\n:::\\\n##") |>
    # replace .large (should also take care of .huge)
    str_replace_all(regex("\\.[lL]arge\\[(\\n+|.+)\\]", multiline = TRUE) ,
                    "\\1") |>
    # Replace slide break and header
    
    str_replace_all("---\\r\\n\\r\\n#(.*)|---\\r\\n#(.*)", "##\\1") |> 
    
    # by end columns (::::) , header and start columns
    str_replace_all(regex("---\\n+^#(.+)", multiline = TRUE),
                    "::::\n\n## \\1\n\n:::: {.columns}") |>
    
    # Hide class template from computation
    str_replace_all(regex("^(class\\:.+)$", multiline = TRUE),
                    "<!--\\1-->") |>
    
    # replace pull-left with column context - narrower than 50
    #str_replace_all(regex("\\r\\n\\.pull-left\\["),':::: {.columns}') |>
     #str_replace_all(regex("\\.pull-left\\["),'::: {.column width = "45%"}') |>

    # suggestion for pull left replacement
       str_replace_all(regex("(?s).pull-left\\["), ':::: {.columns}\n::: {.column width="45%"}') |>
   
    
     str_replace_all(regex(".pull-right\\["), ':::\n:::{.column width="45%"}') #The closed brackets before the .pull-right would have to be removed manually.
    
    # TODO left and right-column treatment
    # TODO knitr change chunks
  
}  

    
#    str_replace(my_line, "(?<=box\\[)(.+?)(?=\\])", "\\2")

  
convert_x2r <- function(rmd_file, outfile_dir = "tmp"){
  # TODO fs::filepath modifications
  qmd_file <- str_replace(rmd_file, ".Rmd$", ".qmd") |> 
    str_replace("lectures", outfile_dir)
  print(qmd_file)
  
  write_file(paste(update_header(rmd_file),
        update_body(rmd_file)), file = qmd_file)
}
  

test <-  update_body("plotting_part1.Rmd")
write_file(test, "plotting_part1_done.qmd")
