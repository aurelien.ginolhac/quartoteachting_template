```{r setup-general, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, dev = "ragg_png")
library(tidyverse)
```

```{r setup-box, include = FALSE}
knitr::opts_template$set(solution = list(box.title = "solution", box.collapse = TRUE,
                                         box.header = list(fill = "#CEFFD0FF", color = "#456646FF"),
                                         box.body = list(fill = "#CEFFD04C", color = "black")))
knitr::opts_template$set(tip = list(box.title = "Tip", box.collapse = NULL,
                                    box.icon = "ion-lightbulb",
                                    box.header = list(fill = "#FFEC8B"),
                                    box.body = list(fill = "#FFEC8B4C", color = "black")))
knitr::opts_template$set(warning = list(box.title = "Warning", box.collapse = NULL,
                                        box.header = list(fill = "#FFAD99"),
                                        box.body = list(fill = "#FFAD994C"),
                                        box.icon = "ion-android-alert"))
knitr::opts_template$set(instructions = list(box.title = "Instructions", box.collapse = NULL,
                                    box.icon = "ion-lightbulb",
                                    box.header = list(fill = "#808080"),
                                    box.body = list(fill = "#8080804C", color = "black")))
# all code with icon for copy
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "<i class=\"fa fa-clipboard\"></i>",
    success_text = "<i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "<i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
```

```{css, echo = FALSE}
h5:before { /* add a quetion mark icon to header5 (question) */
  font-family: "Font Awesome 5 Free";
  display: inline;
  margin-left: -1.3em;
  width: 1.3em;
  font-weight: 900;
  content: "\f059";
}

h5 { /* Header 5 so questions */
  font-size: 18px;
  font-weight: bold;
  margin-top: 1.3em;
  margin-bottom: 1em;
  font-family: "Roboto Condensed", Times, serif;
  color: DarkBlue;
}

.bonus {
  border: 2px solid rgb(188, 143, 143);
  border-radius: 10px;
  background-color: rgba(188, 143, 143, 0.1);
  margin-left: -0.5em;
  width: auto;
  padding-left: 1.1em;
  padding-bottom: 0em;
  margin-bottom: 1em;
  padding-top: 0.5em;
  padding-right: 1em;
}
.bonus > h1:before, .bonus > h2:before, .bonus > h3:before,
.bonus > h4:before, .bonus > h5:before {
  color: rgba(188, 143, 143, 1);
  font-family: "Font Awesome 5 Free";
  display: inline;
  margin-left: -2em;
  padding-left: 25px;
  width: 1.3em;
  font-weight: 900;
  content: "\f059";
}

```

```{css, echo = FALSE}
# from site.css
# /* Adapted from the bigdata.uni.lu website... */
#/* It would be nice if uni.lu could provide css and html code snippets */

ul.social-links {
  width: auto;
  margin: 0;
  padding: 0;
  float: right;
}

ul.social-links li {
  display: inline-block;
  list-style-type: none;
}

ul.social-links li a {
  margin-right: 10px;
}

ul.social-links li a, ul.social-links li a:hover {
  color: #888888;
}

.footer {
  border-top: 1px solid #444;
  padding-top: 15px;
  margin-top: 10px;
}

.footer .row {
  margin-bottom: 10px;
}

.footer img {
  max-height: 42px;
}

.footer a:focus {
  outline: 0;
}

/* Adapting the rmarkdown bootswatch theme to the presence of the logo */
/* and fix some colours to fit the uni.lu template */

html {
  overflow-y: scroll;
}

html body {
  padding-top: 76px;
}

.navbar .container {
  min-height: 65px;
}

.container > .navbar-collapse {
  margin-top: 15px;
}

body .navbar-default {
  background-color: white;
}

body .navbar-default .navbar-nav > li > a {
  /*text-transform: uppercase;*/
  font-size: 16px;
}

.container {
  margin-top: 10px;
}

.navbar-brand img {
  height: 60px;
  width: auto;
  margin-right: 15px;
}
.navbar-brand {
  padding: 0 15px;
}

a.navbar-brand {
  outline: 0;
}

.navbar a {
  outline: 0;
}

.file-list {
  border-left: 5px solid gray;
  margin: 20px;
  padding: 5px;
}

.file-list li {
  list-style-type: none;
  font-size: 18px;
  margin: 10px 5px;
}

/* from https://stackoverflow.com/a/13354689 */
/* plus fix from https://stackoverflow.com/a/48004111/1395352 */
.file-list li:before {
  font-family: "Font Awesome 5 Free";
  display: inline-block;
  margin-left: -1.3em;
  width: 1.3em;
  font-weight: 900;
}

.tutorial li:before {
  content: "\f0c3";
}

.lecture li:before {
  content: "\f02d";
}

pre code {
  overflow: auto;
  word-wrap: normal;
  white-space: pre wrap;
}

```
