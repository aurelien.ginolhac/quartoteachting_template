---
title: "Reproducible manuscripts"
date: today
date-format: "dddd, [the] D[<sup style='font-size:65%;font-style:italic;'>th</sup>] [of] MMMM, YYYY"
institute: Rworkshop
author: "Aurélien Ginolhac"
format:
 unilu-theme-revealjs:
    progress: true
    code-line-numbers: false
    toc: true
    toc-title: "[{{< fa regular bookmark >}}]{.red} Contents"
    toc-depth: 1
    template-partials:
      - toc-slide.html
    include-after-body: toc-add.html
echo: true
---



```{r libs, include=FALSE}
knitr::opts_chunk$set(warning = FALSE, comment = '')
library(countdown)
library(tidyverse)
library(datasauRus)
```

## Introduction

 

:::: {.columns}

 

::: {.column width="45%"}

```{=html}

<div id='toc'></div>

```

:::

 

::: {.column width="45%"}

### Motivation



 

:::

 

::::



## Learning objectives

::::{.columns}
::: {.column width="50%"}
:::{.callout-tip icon=false}
### {{< fa building-columns >}} You will learn to 


- Learn about the reproducible reports and manuscripts

- Use the **markdown** syntax
- Create **Rmarkdown** documents
- Getting familiar with
    + Creating your documents
    + Inserting code
    + Inserting the bibliography
    + Build your final document
    
:::
    
:::
::: {.column width="50%"}
:::{.callout-tip icon=false}

### {{< fa receipt >}} Literate programming 
 

* Computer science: How to write readable and documented code? - Documentation systems

* Data science: How to support the the analysis with code? 
* Academic data analysis: How to ensure that conclusions in a manuscript are backed-up by computations?

:::
:::
::::


## Merging text and code

::::{.columns}
::: {.column width="50%"}
![](img/rmarkdown_output.png){fig-align="center”}


:::
::: {.column width="50%"}
### Motivation


- Write repeating reports with changing numbers, e.g. from a database
- Complete reproducibility with computations in text
- Keep track of your analyses
- Comment/describe each step of your analysis
- Export a single code/text document to various output formats (PDF, HTML...) 
- Use text files that can be managed by a version control system (like [git](https://git-scm.com/))
- Reuse text and code in different environments (website, book, presentation)
:::
::::


## Short history of text-code integration tools


Name | Year | Code | Text | Main usage
----|------|-------|------|----------
Literate programming | 1984 | C/C++  | | Code documentation
`sweave` | 2003| R  | $LaTex$ | Scientific manuscripts
`RMarkdown`  | 2012 |R | Markdown and R | Manuscripts, reports, presentations 
Jupyter notebooks | 2014 | Python | Markdown |  Exploratory analysis
Quarto | 2022 | R, Python, JS, Julia | Markdown | All of the above





## Rmarkdown and Quarto with R

![](img/markdown_hedgehog.png){fig-align="center”}


:::{.aside}
Credit: Artwork by [Allison Horst](https://github.com/allisonhorst)
:::


## Quarto and RMarkdown: Structure

::::{.columns}
::: {.column width="50%"}

![](img/markdown_rmd_example.png)


:::
::: {.column width="50%"}
:::{.callout-tip}
#### YAML header

- To define document wide options
- _title_, _name_, ...
- Quarto and RMarkdown have different header options
:::


:::{.callout-tip}
#### Markdown
- Markdown syntax to write your descriptions, remarks
- Literate programming
:::

:::{.callout-tip}
#### Chunks

- Code to be interpreted by _R_
:::

:::
::::






## Markdown

**Markdown** is used to **format text**.

::::{.columns}
:::{.column width="50%"}

:::{.callout-tip}
#### Markup language

- Such as `XML`, `HTML`
- A coding system used to structure text
- Uses markup tags (_e.g._ `<h1></h1>` in `HTML`)
:::

:::
::: {.column width="50%"}

:::{.callout-tip}
#### HTML example

```
<!DOCTYPE html>
<html>
<body>

<h1>This is a heading</h1>

<p>This is some text in a paragraph.</p>

</body>
</html>
```
:::

:::
::::


::::{.columns}
:::{.column width="50%"}

:::{.callout-tip}
#### **Lightweight** markup language
- Easy to read and write as it uses simple tags (_e.g._ `#`)
:::

:::
::: {.column width="50%"}

:::{.callout-tip}
#### MD example


```
# This is a heading

This is some text in a paragraph
```
:::

:::
::::




## Common text formatting **tags**

::::{.columns}
::: {.column width="50%"}

:::{.callout-tip}
#### Headers

- **6** levels are defined using `#`, `##`, `###` ...
- From BIG to small
:::

:::
::: {.column width="50%"}
:::{.callout-tip}
#### Text style

- **bold** (`**`This will be bold`**`)
- *italic* (`*`This will be italic`*`)
- ***bold and italic*** (`***`This will be bold and italic`***`)
:::
:::
::::

::::{.columns}
::: {.column width="50%"}

:::{.callout-tip}
#### Links and images

- `http://example.com` is auto-linked 
- `[description](http://example.com)`
- `![](path/to/image.jpg)`
- `![desc](path/to/image.jpg)` for alternative description
:::

:::
::: {.column width="50%"}
:::{.callout-tip}

#### Verbatim code

- `code` (`` `coding stuff` ``)
- Triple backticks are delimiting code blocks:
`````
```
This is *verbatim* code
# Even headers are not interpreted
```
`````
rendered as:
```
This is *verbatim* code
# Even headers are not interpreted
```
:::

:::
::::




## Including `r fontawesome::fa("r-project")` code for Rmarkdown


![](img/markdown_rmd_pipeline.png){fig-align="center”}


::::{.columns}
::: {.column width="50%"}

:::{.callout-tip}
#### Rmarkdown
- Extends markdown
- Place **R code** in **chunks**
- **Chunks** will be **evaluated**
- Can also handle bash; python; css; ...
:::


```{r , echo=FALSE}
htmltools::img(src = "img/logo_rmarkdown.png",
               style = "float:right;height:100px;")
```

:::
::: {.column width="50%"}

:::{.callout-tip}
#### Knitr
- Extracts R chunks
- Interprets them
- Formats results as markdown
- Reintegrates them into the main document (md)

![](img/logo_knitr.png)
:::
:::
::::

:::{.callout-tip}
#### Pandoc

- [pandoc](http://pandoc.org/) converts markdown to the desired document (PDF, HTML, ...)
:::



## Code chunks

::::{.columns}
::: {.column width="50%"}
### Insert a chunk 
shortcut: <kbd>CTRL</kbd> + <kbd>Alt</kbd> + <kbd>I</kbd>:

![](img/markdown_rstudio_chunk_add.png)

:::
::: {.column width="50%"}
### Basic options

- Delimited by **triple backticks tags** (` ``` `)
- **Curly braces** denote the engine evaluating the code and the name
   - `{r}` for `r fontawesome::fa("r-project")`
   - `{python}` for Python
   -  Name of chunk, optional but recommended
   
:::
::::

## Chunk options

::::{.columns}
::: {.column width="50%"}
### RMarkdown

    + `show` or hide the source code (`echo = TRUE)`
    + evaluate it or `not` (`eval = FALSE`)
    + figure size in inches (`fig.width = 7`)

![](img/markdown_chunk_example.png)

:::
::: {.column width="50%"}
### Quarto

 * Chunk options are on new lines, following `#|`
 * Values are not R Booleans and work in any programming context
 * Tab-completion!
 * Document references and labels
] 
 ````verbatim
 `r ''````{r my_chunk3}
#| echo: false
#| eval: false
#| output-location: column

```
````

 

:::
::::


## Inline R code

:::{.callout-tip}
#### Integrate small pieces of _R_ code



Use backticks (`` `    ``) followed by the keyword `r`:

`` `r '\x60r <your R code>\x60'` ``

:::

:::{.callout-tip}
#### Example

 Type in ``1 + 1 = `r '\x60r 1 + 1\x60'` `` 
 
 renders as 1 + 1 = `r 1 + 1` .

  More useful: `` `r '\x60r nrow(swiss)\x60'` ``
 
  renders as `r nrow(swiss)`.
:::




## Your turn

::::{.columns}
::: {.column width="50%"}
### Create and worke with a Quarto or RMarkdown document


* Create a new Quarto/RMarkdown document (empty)
* Toggle between source mode and visual mode 
* Include a code chunk and compute something (`ncol(swiss)` if cannot think of something.
* Render the document by
  + pressing the button 
  * using the <kbd>Ctrl/Cmd</kbd> + <kbd>Shift</kbd> + <kbd>K</kbd> shortcut
  * running `quarto::quarto_render("document.qmd")` or `rmarkdown::render("document.Rmd"))`

:::
::: {.column width="50%"}

![](img/markdown_hedgehog.png)

```{r, echo = FALSE}
countdown::countdown(minutes = 10L, blink_colon = TRUE, update_every = 5,
                     padding = "15px",
                  #   left = "33%",
                     color_finished_background = "red", warn_when = 1L,
                     color_warning_border = "red", 
                     color_finished_text = "time's up!",
                     color_running_background = "#fff5ca")
```

:::
::::

## Popular output formats
:::{.callout-tip}
#### HTML

- Fast rendering
- No need for extra install
- By default embeds binaries (pictures, libraries etc.)
:::

:::{.callout-tip}
#### PDF

- Single file
- Requires $LaTeX$,
  have a look at the **[TinyTeX](https://yihui.name/tinytex/)** package for minimal install

:::

:::{.callout-tip}
#### Word

- Widely used
- Easily editable
- **Collaborate** with people not using Rmarkdown
- Prepare **scientific manuscripts** suitable for submission
:::








## Working in RMarkdown

::::{.columns}
::: {.column width="50%"}

### Special chunk named `setup`

To load packages / functions silently before any other chunk executions

````markdown
`r ''````{r setup, include = FALSE}
library(tidyverse)
source("R/helpers.R")
```

````

### Stop the knitting process gracefully

**Stop** the knitting at a specific location in your Rmarkdown document for debugging / fast rendering.
Here optionally named `early_stop`

````{verbatim}
`r ''````{r early_stop}
knitr::knit_exit()
```
````

You may want to add the chunk option `echo = FALSE`

:::
::: {.column width="50%"}
### Caching long computations
Recommended only for "expensive" chunks, set `cache = TRUE` 

``` `{verbatim}
`r ''````{r caching, cache = TRUE}
# big computations

```
````

The `targets` package manages **dependencies** and re-reruns only **outdated** steps.

```{r, echo = FALSE}
htmltools::img(src = "https://docs.ropensci.org/targets/reference/figures/logo.png",
               style = "float:right;height:150px;")
```

:::
::::

## Basic tables

::::{.columns}
::: {.column width="50%"}
### Basic tables created from scratch


````{verbatim}
Dataset   | # rows| Description
----------|-----:|-----
swiss     | 47   | Swiss  Indicators
datasaurus| `r nrow(datasaurus_dozen)` | Example
````

is rendered as

Dataset  | # rows| Description
---------|-----:|------
swiss     | 47  | Swiss Fertility and Socioeconomic Indicators
datasaurus| `r nrow(datasaurus_dozen)` | Example 



Note that numbers have to be manually aligned.

:::
::: {.column width="50%"}

### Tables from tibbles with `knitr::kable()`
Data from tibbles can be formatted for printing with paging etc.


```{r, echo= "TRUE"}
sw_db <- as_tibble(swiss, rownames = "Province") |>
  select(Province:Catholic, 
         -starts_with("E")) |>
  slice_head(n = 5) 

swiss_cap <- "Swiss Fertility and Socioeconomic Indicators (1888)"
knitr::kable(sw_db, caption = swiss_cap)
```


:::
::::



## Pretty tables

::::{.columns}
::: {.column width="50%"}
:::{.callout-tip icon=false}
#### {{< fa table >}} Many table packages 
 
The `knitr::kable()` functionality is sufficient for explorative analysis; for production, you want more control over tables.

* [huxtable](https://hughjonesd.github.io/huxtable/huxtable.html)
* [flextable](https://davidgohel.github.io/flextable/articles/overview.html)
* [stargazer](https://cran.r-project.org/web/packages/stargazer/vignettes)
* [pander](https://www.r-project.org/nosvn/pandoc/pander.html)
* [tables](https://cran.r-project.org/web/packages/tables/index.html)
* [gt](https://gt.rstudio.com/)

:::

Not all packages do everything well and the field is rapidly moving. 

Check the [blog post of David Hugh-Jones](https://hughjonesd.github.io/huxtable/design-principles.html), creator of Huxtable for a survey and functionalities.

:::
::: {.column width="50%"}

### Huxtable

```{r, message=FALSE, warning = FALSE}
library(huxtable)
hux(sw_db) |> 
  set_bold(row = 1, col = everywhere) |> 
  set_width(0.6) |> 
  set_background_color(evens, 
                       everywhere, 
                       "lightgreen")
```


```{r, echo = FALSE}
huxtable::hux_logo()
```

:::
::::




# {{< fa building-columns >}} Scientific publishing with Markdown {style="text-align:center;"background-color="#00A4E1"}




## Layout control

::::{.columns}
::: {.column width="50%"}
### {{< fa image >}} Image placement 

 * Basic imageplacement with
 
 ```![](path/to/image)``` has 
 
 no control over position and size.
 
 * That is good! 
 * Journals typically want figures separately anyway.
 * Quarto has good options for placing [figures](https://quarto.org/docs/authoring/figures.html).
 
:::
::: {.column width="50%"}

### Options to control `r fontawesome::fa("code")`

```
![Image description](figure.png){fig-align="left"}
```

### `:::` creates sections
`<div>` elements in HTML.

````
```
::: {layout-nrow=2}
![Image description](figure.png)

![Image description](figure1.png)


:::
```
````
:::
::::


## Equations using $LaTeX$ syntax
::::{.columns}
::: {.column width="50%"}

:::{.callout-tip}
#### How to add equations

 * Enclose in `$` for in-line equations
 * `$a^2+b^2=c^2$` renders as $a^2+b^2=c^2$.

 * Double (`$$`) for separate equations.
```
$$G_{\mu v}=8 \pi G (T_{\mu v} + \rho _\Lambda \ g_{\mu v}) $$
```

.center[yields]

$$G_{\mu v}=8 \pi G (T_{\mu v} + \rho _\Lambda \ g_{\mu v})$$

* No need for direct interaction with $LaTeX$,  `pandoc` is taking care. 
* Numbering of equations is possible but requires bookdown pages and PDF output.

:::
:::


::: {.column width="50%"}

:::{.callout-tip}
#### Tables

* Complex $LaTeX$ arrangements can be used as alternatives for builtin tables.
```
$$\begin{array}{ccc}
x_{11} & x_{12} & x_{13}\\
x_{21} & x_{22} & x_{23}
\end{array}$$
```

$$\begin{array}{ccc}
x_{11} & x_{12} & x_{13}\\
x_{21} & x_{22} & x_{23}
\end{array}$$
:::

:::{.callout-tip}
####  Potential pitfalls

 * MathJax needs to load, unstable internet connections can intervene
 * The Markdown/ $LaTeX$ mix is sensitive to spaces.
 
:::
:::
::::








## Bibliography

:::{.callout-tip}
#### Supported formats


- Use it with your EndNote or Zotero database:
- **BibLaTeX**, **BibTeX**, **EndNote**, **EndNote XML**, MEDLINE, ISI, MODS, RIS, Copac, JSON citeproc
:::

:::{.callout-tip}
#### Styles


- uses citation style language (`csl`) files
- have a look at:
    + <https://www.zotero.org/styles>
    + <https://github.com/citation-style-language/styles>
    
:::




# Bibliography

::::{.columns}
::: {.column width="50%"}

:::{.callout-tip}
#### Configuring your bibliography

- Setup in the yaml header 
- Insert citations using the [pandoc](https://pandoc.org/MANUAL.html#citations) 
syntax: `[@citation-key]`
:::
```
---
title: "Sample Document"
output: html_document
bibliography: bibliography.bib
csl: nature.csl
---

Barnacles are boring[@darwin].
```

:::{.callout-tip}
#### Using Zotero as a reference manager 
- Try the [Better Bib(La)TeX](https://github.com/retorquere/zotero-better-bibtex) plugin and adjust more preferences

:::
:::

::: {.column width="50%"}


:::{.callout-tip}
#### Article templates
The `rticles` package creates
article templates for journals.
![](img/rticles_template.png)

:::
:::
::::



## Your turn

::::{.columns}
::: {.column width="50%"}

:::{.callout-tip}
#### Exercise continued 

* Check that your RMarkdown document builds
* Commit your RMarkdown document to the git repository
* Push the changes to your github classroom repository

* Optional exploration 
   - Include a formula `dS = \frac{\delta Q}{T}` 
   - Insert a table
   - Print a table with kable
   - Add a bibliography
   
* After your done, commit and push 

:::
:::


::: {.column width="50%"}


![](img/markdown_hedgehog.png)

```{r, echo = FALSE}
countdown::countdown(minutes = 10L, blink_colon = TRUE, update_every = 5,
                     padding = "15px",
                   #  left = "33%",
                     color_finished_background = "red", warn_when = 1L,
                     color_warning_border = "red", 
                     color_finished_text = "time's up!",
                     color_running_background = "#fff5ca")
```

:::
::::

## Lessons learned
::::{.columns}
::: {.column width="50%"}
:::{.callout-tip}
#### Experience

Eight manuscripts authored, computed rendered using Rmarkdown

 - Initially, we kept text and analysis code together
 - Hard to organize - abstract already contains conclusions
 - Eventually, all code was one big chunk in the Rmarkdown doc
 
:::
:::
::: {.column width="50%"}
:::{.callout-tip}
#### Our `standard` setup

Code follows the data life cycle, e.g. using scripts to

1.  `Import`
2.  `Transform`
2.  `Model`
 
Controlled by another script (e.g. using Make, or runner script)

:::
:::
::::
:::{.callout-tip}
#### Proposed organization


 * Explore your dataset in the context of an Rmd document
 * Move production code to a script, e.g. in a directory called `R`

Better: use the`targets`that extends the concept of reproducible workflows.

:::




## Alternatives and outlook
::::{.columns}
::: {.column width="40%"}
:::{.callout-tip}
#### R Notebooks 

Notebooks are another way of bringing together markdown-formatted text and code.

In RStudio's environment, they use the same syntax; the only change is in the YAML header. 

*Preview* shows you a rendered HTML copy of the contents of the editor. 

Consequently, unlike *Knit*, *Preview* does not run any R code chunks. 

Instead, the output of the chunk when it was last run in the editor is displayed.

Notebooks feel more interactive as the results are presented inline and cached.

You can `knit` a Notebook which will convert it to a regular RMarkdown document.
:::
:::
::: {.column width="30%"}

:::{.callout-tip}

#### Rerunning chunks in arbitrary order breaks reproducibility!

```{r , echo=FALSE}
htmltools::img(src = "img/notebooks_horror.png",
               style = "float:right;height:300px;")
```

:::
:::



::: {.column width="30%"}
:::{.callout-tip}
#### Quarto
 * Supports Python, R, Observable JS and Julia
 * Better integration with other editors like VS Code.
 * Additional markdown tags 
  - Page breaks in HTML
 * Advanced layout features
 * Unites features includes Notebooks in a reproducible manner
 
 ![](https://quarto.org/quarto.png){fig-align="center”}

:::

:::
::::

## Before we stop

::::{.columns}
::: {.column width="50%"}

:::{.callout-tip icon=false}
#### {{< fa user-graduate >}} You learned to

- What is `Rmarkdown` (`Rmd`)
- Basic syntax of `Markdown`
- `knit` your `Rmd` to different output formats
- Styling tables
- Bibliography integration
:::


:::{.callout-tip icon=false}
#### {{< fa book-bookmark >}} Further reading 

- http://quarto.org
:::


:::
::: {.column width="50%"}

:::{.callout-tip icon=false}
#### {{< fa handshake-angle >}} Acknowledgments  
* Xie Yihuie (Creator)
* Alison Hill
* Hadley Wickham
* Artwork by [Allison Horst](https://twitter.com/allison_horst)
* Jenny Bryan

#### Contributors
* Eric Koncina (initial content)
* Veronica Codoni (major overhaul)
* Roland Krause ( $LaTeX$, bibliography)
* Aurélien Ginolhac (Chunk options)

:::
:::
::::


### Thank you for your attention!



