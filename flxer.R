library(tidyverse)
library(flexo)



my_file <- read_file("intro.Rmd")


### problem: only complete matches, no partial 
regexes <- c(
  slide_sep   = "---", 
  line_break = "\\n", # might or might not be needed
  
  class_style = "class\\:([^\\n\\[\\]]+)",
  img = '\\!\\[\\]\\((.+)\\)', # only extracts the actual images, strips the brackets 
   code_chunk  = '(```\\{[^`]+```)',
   header = "\\#.+",
#   link = '\\[[^\\]]+\\]\\([^\\)]+\\)', 
   div_col_left = '.pull-left\\[',
   div_col_right = '.pull-right\\[',
   div_flex ='.flex\\[',
#   div_open    = "[^ \\n\\[]*\\[", # everything before the [
open_blue_box = ".*?bg-washed-blue.+?\\[", 
open = ".*?\\[", 
   content     = "[^\\]]+",

   close   = "\\]",

#   #code_chunk_end = '```',
   fontawesome = '`r fontawesome\\:\\:?fa\\((.+)\\).*`',
 # content = "[^\\]]+" , 
#fullstop    = "\\.",
  #
   comma       = ","
)

my_stream <- flexo::lex(my_file, regexes)

df_token <- tibble(names = names(my_stream),
                   content = unname(my_stream),
                   result = unname(my_stream))

t_stream <- TokenStream$new(my_stream[names(my_stream)!="line_break"]) # R6 class


#tmp[names(tmp) %in% c("div_col_left", "div_col_right")]

# parse slide 
parse_slide <- function(stream) {
  div_level <- 1
  stream$assert_name('slide_sep') # only element separating slides in xaringan
  slide_content <- stream$consume_until(name = 'slide_sep', inclusive = FALSE)
  elem <- stream$read(1)
  if(str_detect(names(elem[1]), "^div")){
    append(div_level + 1)
  } 
  slide_content
  #setNames(list(list(slide_content)), identifier)
}

# TODO: add print column to df_token
# How to keep track how many we divs have been opened and closed
# div level (inside a slide)
