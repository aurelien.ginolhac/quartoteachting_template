---
title: "Data wrangling"
date: today
date-format: "dddd, [the] D[<sup style='font-size:65%;font-style:italic;'>th</sup>] [of] MMMM, YYYY"
institute: Rworkshop
author: "Aurélien Ginolhac"
format:
 unilu-theme-revealjs:
    progress: true
    code-line-numbers: false
    toc: true
    toc-title: "[{{< fa regular bookmark >}}]{.red} Contents"
    toc-depth: 1
    template-partials:
      - toc-slide.html
    include-after-body: toc-add.html
echo: true
---


```{r libs, include=FALSE}
library(countdown)
library(flipbookr)
library(tidyverse)
library(fontawesome)
```
## 

 

:::: {.columns}

 

::: {.column width="45%"}

```{=html}

<div id='toc'></div>

```

:::

 

::: {.column width="45%"}

### Motivation



 

:::
::::


## Introduction
::::{.columns}
::: {.column width="33%"}
:::{.callout-tip icon=false}
#### {{< fa gear >}} Data munging 

 * Preparing data is the most time consuming part of data analysis.
 * Individual steps might look *easy*.
 * Essential part of *understanding* the data you're working with.
 * Additional data preparation before modeling is impossible to avoid. 
 
:::
:::
::: {.column width="33%"}
:::{.callout-tip icon=false}
#### {{< fa toolbox >}} At a glance 

`dplyr` is a tool box for working with data in *tibbles*, offering a unified language for operations
scattered through base R.

![](https://raw.githubusercontent.com/tidyverse/dplyr/master/man/figures/logo.png){fig-align="center”}
 
:::
:::
::: {.column width="33%"}

:::{.callout-tip icon=false}
#### {{< fa list >}} Key operations 
![](img/dplyr_schema_vaudor.png)
:::

:::{.aside}
source: [Lise Vaudor](http://perso.ens-lyon.fr/lise.vaudor/dplyr/)</span>
:::
:::

::::


## This lecture
::::{.columns}
::: {.column width="30%"}
:::{.callout-note icon=false}
#### {{< fa table >}} Example data 

Van 't Veer, Anna; Sleegers, Willem, **2019**, "Psychology data from an exploration of the effect of anticipatory stress on disgust vs. non-disgust related moral judgments". _Journal of Open Psychology Data_.

 * Data is not really *tidy*. 
 * Typical data you might see in the wild.
 
:::
:::
::: {.column width="30%"}
:::{.callout-note icon=false}
#### {{< fa building-columns >}} Learning objectives 
   + Learn the **grammar** to operate on rows and columns of a table
   + Selection and manipulation of 
      - observations, 
      - variables and 
      - values. 
   +  Grouping and summarizing
   
   + Joining and intersecting tibbles
   + Pivoting column headers and variables
 
:::  
:::

::: {.column width="40%"}
:::{.callout-note}
#### Key operations 

![](img/dplyr_schema_vaudor.png)

:::
::: {.aside}
source: [Lise Vaudor](http://perso.ens-lyon.fr/lise.vaudor/dplyr/)
:::

:::
::::

## `dplyr`  Introduction: [Cheat sheets](https://www.rstudio.com/resources/cheatsheets/)



![](img/dplyr_cheatsheet.png){fig-align="center”}






## Using the `dplyr` package 

::::{.columns}
::: {.column width="33%"}
:::{.callout-warning}
#### Do not use these packages! 

* `dplyr` supersedes previous packages from Hadley Wickham.
    + `reshape`
    + `reshape2` 
    + `plyr`
 
* All functionality can be found in `dplyr` or `tidyr`.

:::
:::


::: {.column width="33%"}
:::{.callout-note}
#### dplyr *current version is 1.1.0*

 
 * `dplyr` has seen many changes. 
 
 * Watch out for deprecated examples on Stack overflow!

 * Code will break sooner or later (you might get lucky)
 
 * But handled with clear [lifecycle stages](https://lifecycle.r-lib.org/articles/stages.html) now
 
 * Changes are generally introduced to simplify operations.
 
:::
:::


::: {.column width="33%"}
:::{.callout-note} 
#### Loading `dplyr`

```{r, eval = FALSE}
library(dplyr) # AND
library(tidyr)

# OR (recommended here) 

library(tidyverse)
```
:::
:::
::::

## Preparing the data

::::{.columns}
::: {.column width="50%"}
:::{.callout-note}
### Description

Van 't Veer, Anna; Sleegers, Willem. (2019) "[Psychology data from an exploration of the effect of anticipatory stress on disgust vs. non-disgust related moral judgments](http://doi.org/10.5334/jopd.43)". *Journal of Open Psychology Data*. `r fa("creative-commons")` `r fa("creative-commons-zero")`

 * Moral dilemmata (trolley problem, survival after plane crash, etc. `moral`)
 * Standard questionnaires 
    + Private Body Consciousness (`PBC`, range 0 - 4)
    + Rational-Experiential Inventory (`REI`, range 1 - 5)
    + Multidimensional Assessment of Interoceptive Awareness (`MAIA`, range 0 - 5 )
    + State Trait Anxiety Inventory (`STAI`)
    
:::
:::



::: {.column width="50%"}

```{r, eval = FALSE}
judge_url <- "https://biostat2.uni.lu/practicals/data/judgments.tsv"
judgments <- readr::read_tsv(judge_url)
# https://dataverse.nl/api/access/datafile/11863
#"https://biostat2.uni.lu/practicals/data/judgments.tsv")

```
:::
::::


## Your turn! {.slide-practical}


Load the data into your RStudio session if you wish to follow along

```{r, echo = FALSE}
countdown::countdown(minutes = 2L, blink_colon = TRUE, update_every = 5,
                     padding = "15px",
                     left = "42%",
                     color_finished_background = "red", warn_when = 1L,
                     color_warning_border = "red", 
                     color_finished_text = "time's up!",
                     color_running_background = "#fff5ca")
```

```{r}
judgments <- readr::read_tsv("https://biostat2.uni.lu/practicals/data/judgments.tsv", show_col_types = FALSE)
judgments
```




# Inspecting tibbles{style="text-align: center;"background-color="#00A4E1"}



## Inspect tibbles with `glimpse()`

::::{.columns}
::: {.column width="50%"}

 Column-wise description 

Shows some values and the type of **each** column. 

The *Environment* tab in RStudio tab does it too. 

Clicking object `judgments` triggers `View()`.

Similar to the `utils::str()` function

:::

::: {.column width="50%"}


```{r, col_width = 80}
glimpse(judgments)
```


:::
::::

## Selecting *columns*
::::{.columns}
::: {.column width="50%"}
### `select()`

- Select the columns you want: `select(tibble, your_column1, ...)`

```{r, title = "select example", class = "offset-1"}
select(judgments, gender, age, condition)
```
:::

::: {.column width="50%"}

:::{.callout-warning}
### Warning

The `biomaRt` package of Bioconductor (amongst others) provides a `select()` function. If loaded, we need to address the `dplyr`-package using `::`!
:::

```{r}
dplyr::select(judgments, age, gender)
```

:::
::::



## Reminder `tidyselect`

::::{.columns}
::: {.column width="50%"}
:::{.callout-tip}
#### Helper function 
To select columns with names that:

 * `contains()` - a string
 * `starts_with()` - a string
 * `ends_with()` - a string
 * `any_of()` - names in a character **vector**
 * `matches()` - using regular expressions
 * `everything()` - all **remaining** columns
 * `last_col()` - last column
:::

:::{.callout-warning}
#### Warn 
Avoid selecting columns by index!

To ensure reproducibility select columns by **name**
:::
:::



::: {.column width="50%"}


```{r}
select(judgments, starts_with("moral"))
```

:::
::::





## Combining helpers 

::::{.columns}
::: {.column width="50%"}
:::{.callout-tip}
#### Remark

Helpers are found in several functions, _e.g._ `across()`.
:::
:::

::: {.column width="50%"}

```{r}
select(judgments, ends_with("date"), contains("dilemma"))
```
:::
::::



## Selecting *columns* using `select()` 

::::{.columns}
::: {.column width="50%"}

:::{.callout-tip}
#### Negative selection 
Drop columns by **negating** their names **`-`**

Works with the `tidyselect` helper functions.

:::
:::



::: {.column width="50%"}
```{r}
select(judgments, 
       -gender, 
       -starts_with(c("STAI", "REI")),
       -ends_with("id"))
```
:::
::::



## Filtering for rows: `filter()`

::::{.columns}
::: {.column width="50%"}

Let's take a look at all the data that was excluded. `exclude` is coded as $0=include$ and $1=exclude$. 

```{r}
filter(judgments, exclude == 1) 
```
:::

::: {.column width="50%"}


![](img/dplyr_filter.jpg)

:::{.aside}
Artwork by: [@allison_horst](https://github.com/allisonhorst/stats-illustrations)
:::
:::
::::



## Filtering rows 

::::{.columns}
::: {.column width="50%"}
### Multiple conditions: AND 

- **comma** separated conditions are equivalent to `&` (**AND**).
- Filter for females older than 20.

```{r, col_width = 80}
filter(judgments,
  age > 20,
  gender == "female")
```
:::

::: {.column width="50%"}
### Multiple conditions: OR 

- **vertical bar (`|`)** separated conditions are combined with OR.
- Filter females **or** age > 20 (so males too)

```{r}
filter(judgments,
  age > 20 |
  gender == "female")
```
:::
::::



## `relocate()`

- Only present since `dplyr` version **1.0**

- `.before` and `.after` for fine placement. Works also with `mutate()`

:::{.center}
```{r, echo = FALSE}
htmltools::img(src = "img/dplyr_relocate.png",
               style = "height:500px;")
```
:::
:::{.aside}
Artwork by: [@allison_horst](https://github.com/allisonhorst/stats-illustrations)
:::



## Filtering **out** rows

::::{.columns}
::: {.column width="50%"}

:::{.callout-tip}
#### Row vs column selection

- `filter()` acts on rows
- `select()` acts on columns
:::
:::

::: {.column width="50%"}


- Remove excluded participants
- Combine with `relocate()` to place `mood` columns first

```{r, tibble_rows = 3, col_width = 80}
filter(judgments, exclude == 0) |> 
  relocate(contains("mood"))
```

:::
::::




## Filter rows, helper `between()`

```{r}
judgments |>
  filter(between(mood_pre, 40, 60)) |> 
  select(age, gender, condition, mood_pre)
```



## Set operations with `filter()` 

- The two below are equivalent <font color="red">**Never**</font>
 use `== x` where `x` is a vector of length `> 1` (Will be error in R4.2)
- For larger operations use filtering joins such as `semi_join()`. 
- Below, `tidyselect` helper **`:`** for a range of columns
 
::::{.columns}
::: {.column width="50%"}
 

```{r, row = TRUE, title = "is.element()"}
judgments |>
  filter(is.element(start_date, c("11/3/2014", "11/5/2014"))) |>
  select(start_date:age)
```
:::



::: {.column width="50%"}


```{r}
judgments |>
  filter(start_date %in% c("11/3/2014", "11/5/2014")) |>
  select(start_date:age)
```
:::

::::



## Filter out rows that are unique: `distinct()`

::::{.columns}
::: {.column width="50%"}

### Do we have different start / end dates?

```{r}
select(judgments, start_date, end_date)
```

Too many identical rows

:::



::: {.column width="50%"}

### Use `distinct()` to remove duplicated rows:

```{r}
judgments |>
  filter(exclude == 0) |> 
  select(start_date, end_date) |> 
  distinct()
```

- Also possible (except columns order):
```{r, eval = FALSE}

judgments |>
  filter(exclude == 0) |> 
  distinct(start_date, end_date)
```

:::
::::


## Sort columns: `arrange()` 

::::{.columns}
::: {.column width="50%"}
### A nested sorting example

1. Sort by `age`
2. Within each group of `age`, sort by `mood_post`

```{r}
judgments |> 
  arrange(age, mood_post) |> 
  select(subject, age, mood_post)
```
:::

::: {.column width="50%"}

### Reverse sort columns 

- Use `arrange()` with the helper function `desc()`
- For example, *oldest* participant first

```{r}
judgments |> 
  arrange(desc(age), mood_post) |> 
  select(subject, age, mood_post)
```
:::
::::



## Verbs to inspect data


### Summary

- `glimpse()` to get an overview of each column's content
- `select()` to pick and/or omit columns
    + helper functions
- `relocate()` re-arrange columns order
- `filter()` to subset
    + AND/OR conditions (`,`, `|`)
- `arrange()` to sort
    + combine with `desc()` to reverse the sorting



![](img/dplyr_data_cowboy.png){.absolute top=250 left=1100 width="350" height="350"}





:::{.aside}
Artwork by: [@allison_horst](https://github.com/allisonhorst/stats-illustrations)
:::




## Your turn!{style="text-align: center;"background-color="#00A4E1"}


#### {{< fa wrench >}} Exercises 

1. Use `glimpse()` to identify columns and column types in `judgments`.

2. Select all columns that refer to the STAI questionnaire.

3. Retrieve all subjects younger than 20 which are in the stress group.
The column for the group is `condition`.

4. Arrange all observations by `STAI_pre` so that the subject with the lowest stress level is on top. 
What is the subject in question?


```{r, echo = FALSE}
countdown::countdown(minutes = 5L, blink_colon = TRUE, update_every = 5,
                     padding = "15px",
                     left = "40%",
                     color_finished_background = "red", warn_when = 1L,
                     color_warning_border = "red", 
                     color_finished_text = "time's up!",
                     color_running_background = "#fff5ca")
```



## Solution
::::{.columns}
::: {.column width="50%"}
```{r}
glimpse(judgments)
```
:::


::: {.column width="50%"}
```{r}
judgments |> 
  select(starts_with("STAI") )
```
:::
::::



## Solution
::::{.columns}
::: {.column width="50%"}
```{r}
judgments |> 
  filter(age < 20, condition == "stress")
```
:::


::: {.column width="50%"}
```{r}
judgments |> 
 arrange(STAI_pre)
```
:::
::::



# Transforming columns{style="text-align: center;"background-color="#00A4E1"}




## Changing column names 

::::{.columns}
::: {.column width="50%"}

### `rename(data, new_name = old_name)`

_to remember the order of appearance, consider `=` as "was"._


```{r}
rename(judgments, 
       done = finished,
       sex = gender)
```
:::


::: {.column width="50%"}
### With a function: `rename_with()`

For the `STAI` columns convert names to lower case

```{r}
rename_with(judgments, 
            stringr::str_to_lower, 
            starts_with("STAI"))
```

:::
::::

## Adding columns: `mutate()`

::::{.columns}
::: {.column width="50%"}
Let's create a new column `mood_change` that describes the change of the mood of 
the participant across the experiment. 

- New column name: `mood_change`
- Computation: subtract `mood_pre` from `mood_post`

```{r}
judgments |>
  mutate(mood_change = mood_post - mood_pre) |> 
  relocate(starts_with("mood"))
```
:::



::: {.column width="50%"}

```{r, echo = FALSE}
htmltools::img(src = "img/dplyr_mutate.png",
               style = "height:400px;")
```



:::
::::



## Within one mutate statement

::::{.columns}
::: {.column width="50%"}
:::{.callout-note}
#### Instant availability  

Use new variables in the same function call right away!
:::
:::

::: {.column width="50%"}

```{r}
judgments |>
  mutate(
    mood_change = mood_post - mood_pre,
    # remove missing data before computation
    mood_change_norm =
      abs(mood_change / mean(mood_change, na.rm = TRUE))) |>
  relocate(starts_with("mood")) |> 
  arrange(desc(mood_change_norm))
```

:::
::::

## Replacing columns

::::{.columns}
::: {.column width="50%"}
:::{.callout-warning}
#### Update

Using existing columns updates their content.
:::

:::{.callout-warning}
If **not using names** actions are used as names **avoid**
:::
:::


::: {.column width="50%"}

### `mutate()` existing columns, centering `mood` columns

```{r}
judgments |>
  mutate(mood_pre = mood_pre / mean(mood_pre, na.rm = TRUE),
         mood_post = mood_post / mean(mood_post, na.rm = TRUE),
         mood_pre / mean(mood_post, na.rm = TRUE)) |> 
  select(starts_with("mood"))
```

:::
::::


## Switch statements `case_when()` and `case_match()` 

::::{.columns}
::: {.column width="50%"}

Categorize `mood_pre`. Tests come **sequentially**

```{r}
judgments |>
  mutate(mood_pre_cat = case_when(
    mood_pre < 25   ~ "poor", 
    mood_pre < 50   ~ "mid", 
    mood_pre < 75   ~ "great",
    mood_pre <= 100 ~ "exceptional",
    TRUE ~ "missing data")) |> 
  select(mood_pre, mood_pre_cat) #|>
 # count(mood_pre_cat)
```

:::


::: {.column width="50%"}



```{r, echo = FALSE}
htmltools::img(src = "img/dplyr_case_when.png",
               style = "height:360px;position: absolute;bottom: 2em;right: 0em;")
```


:::
:::


## `case_match()` version

::::{.columns}
::: {.column width="50%"}

* The column is stated only once.
* `.default` to control the unmatched values
* New in `dplyr` 1.1.0

:::


::: {.column width="50%"}
```{r}
judgments |>
  mutate(mood_pre_cat = case_match(
    mood_pre,
    c(0:24)   ~ "poor", 
    c(25:49)  ~ "mid", 
    c(50:74)  ~ "great",
    c(75:100) ~ "exceptional",
    .default = "missing data")) |> 
  select(mood_pre, mood_pre_cat)
```
:::
::::

## Act on multiple columns at once using `across()`

::::{.columns}
::: {.column width="50%"}
:::{.callout-note icon=false}
#### {{< fa book >}} Usage 
Can be plugged into `mutate()`, `summarise()`...

`across`(<font color="green">ON WHO</font>,<font color="red"> DO WHAT</font>)

- <font color="green">**Columns**</font> selection:
    + Argument `.cols`
    + `tidyselect` *helpers*
    + `everything()` = all columns.
    + **Conditions** (boolean) needs `where()`, `across(where(is.numeric))`

- <font color="red">**Actions**</font> using functions:
    + Argument `.fns`
    + `fun, arg1, arg2`
    + `\(x) fun(x)`, with `placeholder` `x`
    + Multiple functions as arguments need to be wrapped up
    + New column names can be controlled
    
:::
:::


::: {.column width="50%"}

```{r, echo = FALSE}
htmltools::img(src = "img/dplyr_across.png",
               style = "height:400px;")
```



:::
::::

## Examples of `across()` usage

::::{.columns}
::: {.column width="50%"}

### Add 1 to the STAI questionnaire data 

To convert Likert scales 0-4 to 1-5 (same column names)

```{r}
judgments |> 
  mutate(across(contains("STAI"), `+`, 1)) |>
  select(starts_with("STAI"))
```

:::

::: {.column width="50%"}

### To specify different names and not overwrite cols
```{r}
judgments |> 
  mutate(across(starts_with("mood"), scale, 
                .names = "rescale_{.col}")) |>
  select(contains("mood"))
```

:::
::::

## For `filter` the across is renamed to if any or all

::::{.columns}
::: {.column width="50%"}
### Find rows where ANY of mood data columns are missing
```{r}
judgments |> 
  filter(if_any(starts_with("mood_p"), is.na)) |>  #<<
  select(subject, starts_with("mood"))
```
:::


::: {.column width="50%"}
### Find rows where BOTH of mood data columns are missing
```{r}
judgments |> 
  filter(if_all(starts_with("mood_p"), is.na)) |> #<<
  select(subject, starts_with("mood"))
```
:::
::::



## {{< fa magic >}} Using lambdas  

::::{.columns}
::: {.column width="50%"}

:::{.callout-tip}
#### Reminder

`\(x) x + 1` is just a shorthand for

`function(x) {x + 1}`
:::
:::


::: {.column width="50%"}
### Add 1 to the STAI questionnaire data


```{r}
judgments |> 
  mutate(across(contains("STAI"), 
                \(x) x + 1)) |>
  select(starts_with("STAI"))
```

:::
::::

##  Selecting columns with a predicate `where()`

::::{.columns}
::: {.column width="50%"}

### Add 1 to numeric columns for all questionnaire data

- **Predicate** means return `TRUE` or `FALSE`
```{r}
judgments |> 
  mutate(across(where(is.numeric),
                \(x) x + 1)) 
```
:::


::: {.column width="50%"}
:::{.callout-warning}
#### Watch out 
Now we also get `subject` changed!
:::

:::{.callout-tip}
However, .**grouped** variable are **protected**
:::



```{r}
group_by(judgments, subject) |>
  mutate(across(where(is.numeric), 
                \(x) x + 1)) |>
  select(finished, subject) |> 
  head(3L) |> 
  knitr::kable()
```

:::

::::

## More advanced across(): multiple functions

::::{.columns}
::: {.column width="50%"}
### Summarise by the mean of mood
```{r}
summarise(judgments,
          across(starts_with("mood"), 
                 list(mean, sd), na.rm = TRUE)) 
```

Remember what was done in the guided practical `datasauRus`

:::

::: {.column width="50%"}
### Better with naming and function arguments
```{r}
summarise(judgments,
          across(starts_with("moral_dil"), 
                 list(aveg = \(x) mean(x, na.rm = TRUE), 
                      sdev = \(x) sd(x, na.rm = TRUE)))) 
```
:::
::::


## Manipulation by row

::::{.columns}
::: {.column width="50%"}

#### Summing all scores for the STAI questionnaire


```{r}
judgments |> 
  mutate(total_stai = 
           sum(across(contains("STAI")), 
               na.rm = TRUE)) |>
  select(subject, total_stai, contains("STAI"))
```

:::

::: {.column width="50%"}
:::{.callout-tip}
 * `rowwise()` - computation by row
 * `c_across()` - selects columns with `tidyselect`
 
:::
```{r}
judgments |>
  rowwise() |> #<<
  mutate(total_stai = sum(c_across( #<<
    starts_with("STAI")), na.rm = TRUE)) |>
  select(subject, total_stai, contains("STAI"))
```

:::
::::


## Your turn!{style="text-align: center;"background-color="#00A4E1"}



### {{< fa wrench >}} Exercises 

1. Abbreviate the gender column such that only the first character remains

2. Create a new `STAI_pre_category` column.
Use `case_match()` to categorize values in `STAI_pre` as "low", "normal" or "high".
For values < 25 in `STAI_pre` assign "low", for values > 65 assign "high", and for all other values assign "normal".

`Hint:` To easily see the new column, use `relocate()` to move it to the first position of the dataframe.







```{r, echo = FALSE}
countdown::countdown(minutes = 10L, blink_colon = TRUE, update_every = 5,
                     padding = "15px",
                     left = "42%",
                     color_finished_background = "red", warn_when = 1L,
                     color_warning_border = "red", 
                     color_finished_text = "time's up!",
                     color_running_background = "#fff5ca")
```

## Solution
::::{.columns}
::: {.column width="50%"}
```{r}
judgments |> 
  mutate(gender = str_sub(gender, 1,1))

```
:::
::: {.column width="50%"}
```{r}
judgments |>
  mutate(STAI_pre_category = case_match(
    STAI_pre, 
    c(0:24)  ~ "low",
    c(25:65) ~ "normal",
    .default = "high")) |>
  relocate(STAI_pre_category)
```
:::
::::

## Solution
```{r}
judgments |> 
  mutate(across(contains("REI"), 
                \(x) x / 5)) |>
  relocate(contains("REI"))

```




## Next up!
:::{.center}
:::{.Large}
**Grouping and summarising with `dplyr`** 
:::
:::

