```{r setup-lectures, include = FALSE}
knitr::opts_chunk$set(fig.retina = 3, comment = '', dev = "ragg_png") #  dpi is 72 x 3 = 216
# theme from Gaben Buie
library(xaringanthemer) # "gadenbuie/xaringanthemer"
# "gadenbuie/xaringanExtra"
style_mono_accent(
  base_color = "#00A4E1", # LU blue flag
  text_font_size = "0.9rem",
  code_font_size = "0.7rem",
  header_h1_font_size = "2.1rem",
  header_h2_font_size = "1.9rem",
  header_h3_font_size = "1.5rem",
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Fira Mono")
)
xaringanExtra::use_xaringan_extra(c("tile_view", "tachyons", "logo"))
# bring in tachyons https://roperzh.github.io/tachyons-cheatsheet/
xaringanExtra::use_tachyons()
# tile view when press 't'
xaringanExtra::use_tile_view()
# use webcam, on/off with 'w' and press Shift + W to move the video to the next corner.
xaringanExtra::use_webcam()
xaringanExtra::use_share_again()
xaringanExtra::use_search(show_icon = FALSE)
xaringanExtra::use_scribble()
set.seed(1423)
```

```{r, echo = FALSE}
# all code with icon for copy
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "<i class=\"fa fa-clipboard\"></i>",
    success_text = "<i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "<i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
```



```{r, echo = FALSE}
xaringanExtra::use_logo(
  image_url = "img/00/logo-rworkshop.png",
  height = "60px",
  position = xaringanExtra::css_position(bottom = "1em", left = "1em"),
  link_url = "https://rworkshop.uni.lu",
  exclude_class = c("title-slide", "inverse", "hide_logo"))
```

```{r, echo = FALSE}
xaringanExtra::use_progress_bar(location = "top", color = "#00A4E1")
```

```{r, _coursename, echo = FALSE}
course_name <- "Programming with the tidyverse"
course_name <- "rworkshop"

```

```{r, echo = FALSE}
build_commit_table <- function() {
  time_format <- "%Y-%m-%d %H:%M:%S %z"
  build_time <- Sys.time()
  
  git_log <-
    tibble(raw = system("git log -n 1 --date=iso", intern = TRUE)) |>
    separate(raw,
             into = c("desc", "value"),
             extra = "merge",
             fill = "right") |>
    slice(1, 2, 3, 5) |>
    #  mutate(value =replace(value, 4, format(value, time_format))) |>
    add_row(
      desc = c("Build time",
               "Repository URL",
               "Branch"),
      value = c(
        format(build_time , time_format),
        system("git config --get remote.origin.url", intern = TRUE),
        system("git rev-parse --abbrev-ref HEAD", intern = TRUE)
      )
    ) |>
    add_column(
      key  = c(
        "Commit SHA",
        "Commited by",
        "Commit time",
        "Commit message",
        "Built time",
        "Repository URL",
        "Branch"
      )
    )    |>
    dplyr::slice(c(6, 7, 1, 2, 4, 3, 5)) # arrange has not easy custom order
   
   commit_time <- as.POSIXct(pull(git_log, value)[[6]])
  
  if (build_time - commit_time > as.difftime(1, units = "hours")) {
    warning(
      "Build and commit time are more than 1h apart. Did you commit your most recent changes before releasing material to production?"
    )
  }
  
  git_log
}


#' Commit and session informatiomn
#' 
#' Include as
#' ```{r,results = 'asis'}
#' commit_session_info()
#' ```
#' 
#' @return Two Xaringan formated slides containing the build/git information and sessionInfo
#' @export
#'
#' @examples
commit_session_info <- function() {
  cat("\n---\n# Built info\n")
  print(knitr::kable(build_commit_table() |> dplyr::select(key, value), 
  col.names = c("", "")))
  
 cat("\n---\n# Session Information\n")
 sessionInfo()
}
```
