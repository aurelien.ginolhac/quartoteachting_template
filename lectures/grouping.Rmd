---
title: "Data wrangling 2"
subtitle: "`r params$subtitle`"
author: "`r params$author`"
date: "`r params$date`"
params:
  title: "Data wrangling 2"
  subtitle: "Grouping and summarising with `dplyr`"
  date: "8 February 2023"
  author: "Roland Krause"
output:
  xaringan::moon_reader:
    nature:
      highlightLines: true
---
class: title-slide

```{r setup-lecture, child = '_setup_lecture.Rmd'}
```


```{r libs, include=FALSE}
library(countdown)
library(flipbookr)
library(tidyverse)
library(fontawesome)
```

# `r params$title`

## `r params$subtitle`

.center[<img src="https://raw.githubusercontent.com/tidyverse/dplyr/master/man/figures/logo.png" width="100px"/>]

### `r params$author` | `r course_name` | `r params$date`



---

```{r data_loading}
#| include: false
judgments <- readr::read_tsv("https://biostat2.uni.lu/practicals/data/judgments.tsv", show_col_types = FALSE)
judgments
```


---  

# Introduction

.pull-left[
###  Learning objectives `r fontawesome::fa(name = "university")`

.Large[
* Compute groups with `dplyr`
* Create summary statistics in an analysis workflow

]
]

.pull-right[
### Summarising `r fontawesome::fa(name = "receipt")`
 
 .Large[
* Work with summary data by 
 - summarising,
 - adding to the existing tibble or
 - changing the number of rows.


* `dplyr` 1.1.0 features persistent and temporary grouping as well as new functions for summaries.

]

]

???
* Base R is using vectors of factors with `aggregate()`

---

# Counting elements

.Large[How many participants per condition?]

--
.pull-left[

### Replacing calls to `table()` 

```{r}
as.data.frame(table(judgments$condition))
```

Ugly and convoluted. `r fa("poo", fill = "brown")`, first column is factor!.  
]


--
.pull-right[
- `count()` groups by specified columns
- Result is a tibble that can be processed further 
- `sort = TRUE` avoids piping to `arrange(desc(n))`

```{r}
count(judgments, condition,
      sort = TRUE)
```

`count()` is a shortcut for:
```{r, eval = FALSE}
judgments |> 
  group_by(condition) |>
  summarise(n = n(), .groups = "drop")
```

]

---
class: hide_logo

## Summarise data

.pull-left[

### When counting is not enough

```{r}
summarise(judgments,
          min = min(mood_pre, na.rm = TRUE),
          max = max(mood_pre, na.rm = TRUE))
```
- `summarise` returns as many rows as groups - one if no groups.
- `mutate` returns as many rows as given.

```{r}
mutate(judgments,
       min = min(mood_pre, na.rm = TRUE),
       max = max(mood_pre, na.rm = TRUE), .before = 1)
```

]

--

.pull-right[
### If we want the min/max per condition

```{r}
judgments |> 
  group_by(condition) |> 
  summarise(min = min(mood_pre, na.rm = TRUE),
            max = max(mood_pre, na.rm = TRUE))
```
]

---

## Within one summarise statement 

.pull-left[

.bg-washed-yellow.b--gold.ba.bw2.br3.shadow-5.ph3.mt3.mr1.ml1[
.large[
.bbox[Commonly used]

- `n()` to count the number of rows
- `n_distinct()` to count the number of distinct observations - *used inside the dplyr verbs!*
- `first()` to extract the observation in the first position
- `last()` to extract the observation in the last position
- `nth()` to take the entry in a specified position

.bbox[Stats functions]
* `mean()`, `sd()`, etc

]

]
]

.pull-right[

```{r}
summarise(judgments,
          n_rows = n(), 
          n_subject = n_distinct(subject),
          first_id = first(subject),
          last_id = last(subject),
          mean = mean(mood_pre, na.rm= TRUE),
          id_10 = nth(subject, n = 10))
```
]

---

# Persistent of grouping with `group_by()` `r fontawesome::fa("burger")`

.pull-left[
### `group_by()` results in a persistent group
.Large[
* Each call to the tibble will respect the grouping
* Summarize will strip away one level of grouping
]


]

.pull-right[
```{r}
judgments |> group_by(condition)
```

The grouping is indicated in the resulting tibble.

]

---

## Summarising is removing grouping

.pull-left[


```{r}
judgments |> 
  group_by(condition) |> 
  summarise(min = min(mood_pre, na.rm = TRUE))
```
]

.pull-right[
### Grouping removed `r fontawesome::fa("lightbulb")`
Not a concern for grouping by single variables but ...
]

---

## Grouping by more than one variable

.pull-left[
### Peeling effect

For variable grouping, one is .bold[peeled off] from the .bold[right] 

```{r}
judgments |>
  group_by(condition, gender) |> 
  summarise(n = n()) -> n_part

slice_head(n_part, n = 1) 
```


.rbox[`r fa("exclamation-triangle", fill = "yellow")` Most functions in `dplyr` are group-aware!]
]

--

.pull-right[
### No grouping after additional calls to `summarise()`

.large[The remaining `condition` group was peeled off too]
```{r}
summarise(n_part, n = n())
```
]


---

# Manipulate retaining grouping

.pull-left[
.bg-washed-yellow.b--gold.ba.bw2.br3.shadow-5.ph3.mt3.mr1[
.large[
.bbox[`r fontawesome::fa("lightbulb")` Remark ]
Ask explicitly to .bold[ungroup] data
* by `ungroup()` 
* by `.groups` argument to `keep` or `drop` groups.

You can inspect the grouping of complex objects or programmatically by the `` functions
]
]

]
.pull-right[
### How many different answers for a variable by condition and gender?

```{r}
judgments |>
  group_by(condition, gender) |> 
  summarise(n_ans = n_distinct(STAI_post_1_1),
            .groups = "drop")
```   
]

---

##  Arranging values within groups
.pull-left[
### `arrange()` can sort values by multiple columns

.bg-washed-red.b--red.ba.bw2.br3.shadow-5.ph3.mt3.mr1[
.large[.rbox[`r fa("exclamation-triangle", fill = "yellow")` Warning! ]]
.center[
.large[Arranging is ignoring groups!]]
]
]

.pull-right[
```{r}
judgments |> 
  mutate(mood_pre_cat = case_when(
    mood_pre < 25  ~ "poor", 
    mood_pre > 75 ~ "great",
    TRUE ~ "normal")) |> 
  group_by(mood_pre_cat) |> 
  arrange(desc(mood_post))|> 
  select(mood_pre_cat, mood_post) |> 
  distinct()
```
]

---

##  Arranging values within groups (continued.)
.pull-left[
### For `arrange()` and grouping
.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt3.mr1[
.large[.bbox[Solution]
.center[Use `.by_group = TRUE`]]
]
]

.pull-right[
```{r}
judgments |> 
  mutate(mood_pre_cat = case_when(
    mood_pre < 25  ~ "poor", 
    mood_pre > 75 ~ "great",
    TRUE ~ "normal")) |> 
  group_by(mood_pre_cat) |> 
  arrange(desc(mood_post), .by_group = TRUE) |> #<<
  select(mood_pre_cat, mood_post) |> 
  distinct()
```

.large[But, you are better off using:]

```{r, eval = FALSE}
  [...] |>
  arrange(mood_pre_cat, desc(mood_post)) |> #<<
  [...]
```


]

---

# Dealing with multiple return values per group

.pull-left[

- `range()` returns min .bold[and] max
- `summarise()` duplicates the key names
```{r}
judgments |>
  group_by(condition, gender) |> 
  summarise(range = range(mood_pre, na.rm = TRUE),
            n = n(),
            .groups = "keep")
```

]

--




.pull-right[
### Using reframe
```{r}
judgments |>
  group_by(condition, gender) |> 
  reframe(range = range(mood_pre, na.rm = TRUE),
            n = n(),
            .groups = "keep")
```

]

---

## Reframe with more values

.pull-left[
### Return values by group

* `mutate()` requires the same number of rows
* `summarize()` requires one value per group (now)
* `reframe()` accepts arbitrary number of rows per group and returns an ungrouped tibble. 

New in `dplyr` 1.1.0 `r fontawesome::fa("calendar-plus")`

]

### More advanced but useful: 3 quantiles
```{r}
judgments |>
  filter(!is.na(mood_pre)) |> 
  group_by(condition, gender) |>
  reframe(
    quan = quantile(mood_pre,
                    c(0.25, 0.5, 0.75)),
    q = c(0.25, 0.5, 0.75),
    n = n())

```
]

---

# Grouping and `mutate()`

.pull-left[
### Grouping works with other verbs

.Large[
* Most useful with `mutate()`
  - As rows are not collapsed, the data is simply computed as for the groups
* Note that the tibble is grouped after the operation.
]


]


.pull-right[
```{r}
judgments |>
  group_by(condition) |>
  mutate( #<<
    n = n()) |> 
  relocate(condition, n)
```
]

---

# Temporary grouping

.pull-left[
### `dplyr` 1.1.0 introduced the `.by` argument

* Works with `summarize()`, most useful with `mutate()`
* No need to call `ungroup()`
]


.pull-right[
```{r}
judgments  |> 
  mutate(n = n(),
         .by = condition) |> #<<
  relocate(condition, n)
```

]

---

# Summary 

.pull-left[
.flex[
.w-100.bg-washed-yellow.b--gold.ba.bw2.br3.shadow-5.ph3.mt3.mr1[
.bbox[Most commonly used - 80% ]

 - `select()` - columns
 - `filter()` - rows meeting condition
 - `arrange()` - sort
 - `glimpse()` - inspect
 - `rename()` - change column name 
 - `relocate()` - move columns
 - `mutate()` - create columns
 - `case_when()` simplifies if/else/if/else
 - `across()`, `c_across()` - work on >1 column
 - `group_by()`, `ungroup()`, `rowwise()`
 - `summarise()` - group-wise summaries

Source: Lise Vaudor [blog](http://perso.ens-lyon.fr/lise.vaudor/dplyr/)

]
]
]

--

.pull-right[
.flex[
.w-100.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt3.mr1[

.bbox[Comments]

 * Represent the verbs you will use 80% of your time.
Go to the website to see additional functions.
 * `tidyr` and `dplyr` are replacing the `reshape` and `reshape2` packages
- tidy data
    + <http://tidyr.tidyverse.org/>
    + `vignette("tidy-data")`

]]]

---

# Bigger data

### Go for `data.table`


.large[
+ See this interesting [thread](http://stackoverflow.com/questions/21435339/data-table-vs-dplyr-can-one-do-something-well-the-other-cant-or-does-poorly) about comparing `data.table` versus `dplyr` 
 + [`data.table`](https://cran.r-project.org/web/packages/data.table/index.html), see  [introduction](https://github.com/Rdatatable/data.table/wiki) is very efficient but the syntax is not so easy.
 + Main advantage: inline replacement (tidyverse is frequently copying)
 + As a summary:      _tl;dr   data.table for speed, dplyr for readability and convenience_ [Prashanth Sriram](https://www.quora.com/Which-is-better-to-use-for-data-manipulation-dplyr-package-or-data-table-library)
+ Hadley recommends that for data > 1-2 Gb, if speed is your main matter, go for `data.table` 
+ [`dtplyr`](https://dtplyr.tidyverse.org/) is for learning the `data.table` from `dplyr` API input
+ [`tidytable`](https://markfairbanks.github.io/tidytable/) is actually using `data.table` but with `dplyr` syntax
+ `data.table` might be not useful for specialized applications with high volumes such as genomics. 

]

```{r, echo = FALSE}
htmltools::img(src = "https://raw.githubusercontent.com/Rdatatable/data.table/master/.graphics/logo.png",
               style = "height:150px;position: absolute;top: 1em;right: 1em;")
```

---
class:slide-practical

# Your turn!

.w-60.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt3.ml6[
.large[.gbox[Exercises `r fa("wrench")`]]

Use `judgments` to compute basic statistics for all moral dilemma columns considering the conditions:
1. Compute the mean, the median, the standard deviation as well as min and max values.
2. Find meaningful short names for the functions such as `med` for `median()`.
3. Assign `judgments_condition_stats` to the results.

In `judgments`:
1. Find the number of subjects by age, gender and condition, e.g. how many 20 years of age females are in the `stress` group. 
2. Sort the resulting tibble such that the condition that contains the most populous group is sorted first (i.e. `stress` or `control` appear together). 
3. Ensure that the resulting tibble does not contain groups.

]
```{r, echo = FALSE}
countdown::countdown(minutes = 10L, blink_colon = TRUE, update_every = 5,
                     padding = "15px",
                     left = "33%",
                     color_finished_background = "red", warn_when = 1L,
                     color_warning_border = "red", 
                     color_finished_text = "time's up!",
                     color_running_background = "#fff5ca")
```

---
class:slide-practical

# Solution

```{r}
judgments |> 
  group_by(condition) |> 
  summarize(across(starts_with("moral_dilemma"),
                   list(
                     mean = mean,
                     sd = sd,
                     med = median ,
                     min = min,
                     max = max
                   ))) -> judgments_condition_stats

```

```{r}
judgments |> 
  group_by( condition, gender, age) |> 
  summarize(n = n()) |> 
  arrange(desc(n), .by_group = TRUE) |> 
  ungroup()
```

---
# Before we stop

.flex[

.w-50.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt2.ml1[
.gbox[You learned to: `r fa("user-graduate")`]

+ Selection and manipulation of 
   - observations, 
   - variables and 
   - values. 
+  Grouping and summarizing

Next step: .bold[joining tables]
]

.w-50.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt2.ml2[

.bbox[Acknowledgments `r fontawesome::fa("hand-sparkles")`]

* Hadley Wickham
* Lionel Henry
* Romain François
* [Gina Reynolds](https://github.com/EvaMaeRey) for fantastic flipbooks
* [Lise Vaudor](https://twitter.com/allison_horst) nice blog
* [Allison Horst](https://twitter.com/allison_horst) for the great ArtWork
* [Alexandre Courtiol](https://github.com/courtiol/Rguides) for cheatsheets
* Jenny Bryan
* [poorman](https://nathaneastwood.github.io/poorman/) by Nathan Eastwood, a re-implementation of `dplyr` in base only

.bbox[Contributions `r fa("hammer")`]
* Milena Zizovic
* Aurélien Ginolhac
]
]

.w-60.pv2.ph3.mt1.ml6[
.huge[.bbox[Thank you for your attention!]]
]

