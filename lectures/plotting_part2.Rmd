---
title: "Plotting data, part 2"
subtitle: "`r params$subtitle`"
author: "`r params$author`"
date: "`r params$date`"
params:
  title: "Plotting data, part 2"
  subtitle: "with `ggplot2`"
  date: "8 February 2023"
  author: "A. Ginolhac"
output:
  xaringan::moon_reader:
    nature:
      highlightLines: true
---
class: title-slide

```{r setup-lecture, child = '_setup_lecture.Rmd'}
```

```{r libs, include=FALSE}
knitr::opts_chunk$set(warning = FALSE, comment = '')
library(tidyverse)
library(hrbrthemes)
library(flipbookr)     # EvaMaeRey/flipbookr
library(patchwork)
library(ggtext)
library(ggh4x)
library(palmerpenguins)
```

# `r params$title`

## `r params$subtitle`

.center[<img src="img/00/logo_ggplot2.png" width="100px"/>]

### `r params$author` | `r course_name` | `r params$date`


---
class: inverse, center, middle

# Scales catalogue

### 62 functions available!

![](img/00/book.png)

---

# Scales for all


.pull-left[

.large[Functions are based on this .bold[scheme]:]

```
scale_{aes}_{type}()
```

### Arguments to change default

- `breaks`, choose where are labels
- `labels`, usually with package [`scales`](https://scales.r-lib.org/)
- `trans`, to change to _i.e_ `log`


### tidyr `crossing` + glue `glue_data` trick

.Large[To generate all combinations]

]

--

.pull-right[
```{r}
tidyr::crossing(aes = c("fill", "colour", "alpha", "x", "y"),
         type = c("continuous", "discrete", "date")) |>
  glue::glue_data("scale_{aes}_{type}()")
```

```{r}
scales::label_percent()(c(0.4, 0.6, .95))
scales::label_comma()(20^(1:4))
scales::breaks_log()(20^(1:3))
```

]

---
class: hide_logo

# Scales transformation


.pull-left[
### Starwars: who is the massive guy?

```{r, fig.height=4}
ggplot(starwars, aes(x = height, y = mass)) +
  geom_point(size = 2) # from dplyr
```
]

--

.pull-right[

```{r, fig.height=4, warning=FALSE}
ggplot(starwars, aes(x = height, y = mass)) +
  geom_point(alpha = 0.6, size = 2) +
  geom_text(data = \(x) filter(x, mass > 1000),
            aes(label = name), nudge_y = -0.2) +
  annotation_logticks(sides = "l") +
  scale_y_continuous(trans = "pseudo_log")
```

 `pseudo_log` is available through [`scales`](https://scales.r-lib.org/reference/pseudo_log_trans.html).
 
 All geometries `geom_` can take either a `data.frame` or a function, here `geom_text()`.
 
 Native **lambda**, `r fontawesome::fa("r-project", fill = "steelblue")`  **<** 4.1 would be `function(x) filter(x, mass > 1000)`.




]

---
class: center, middle

# Custom colors

![](img/plot_comparison_palette.png)

Better: see [Emil Hvitfeldt repo](https://github.com/EmilHvitfeldt/r-color-palettes/blob/master/type-sorted-palettes.md)

---

# Changing colours for categories


.pull-left[
### Default is `scale_fill_hue()`
```{r, fig.height=4}
filter(starwars, !is.na(gender)) |>
ggplot(aes(y = gender,
          fill = fct_lump_n(species, 8) |>
                 fct_infreq())) +
  geom_bar(position = "fill") +
  scale_fill_hue() +
  labs(fill = "top 8 Species\n(ranked)")
```
]

--

.pull-right[
### Rbrewer is a safe alternative
```{r, fig.height=4}
filter(starwars, !is.na(gender)) |>
ggplot(aes(y = gender,
          fill = fct_lump_n(species, 8) |>
                 fct_infreq())) +
  geom_bar(position = "fill") +
  scale_fill_brewer(palette = "Set1") +
  labs(fill = "top 8 Species\n(ranked)")
```
]

---

# Predefined colour palettes

.left-column[
```{r brewer, eval = FALSE}
library(RColorBrewer)
par(mar = c(0, 4, 
            0, 0))
display.brewer.all()
```
]

.right-column[
```{r ref.label='brewer', fig.height = 6.5, echo = FALSE}
```
]

---


# Colour gradient, for continuous variables


The default gradient generated by `ggplot2` is not very good...
Better off using `viridis` (`scale_color_viridis_c()` `c` for continuous)

.pull-left[

```{r grad_default, fig.height=4}
penguins |>
  ggplot(aes(x = bill_length_mm, 
             y = bill_depth_mm, 
             colour = body_mass_g)) +
  geom_point(alpha = 0.6, size = 5)
```
]

--

.pull-right[
```{r, fig.height=4}
penguins |>
  ggplot(aes(x = bill_length_mm, y = bill_depth_mm, 
             colour = body_mass_g)) +
  geom_point(alpha = 0.6, size = 5) +
  scale_color_viridis_c()
```
]

--

.plot-callout[
![](img/plot_culmen_depth.png)
]

---

# Viridis palettes


```{r, echo = FALSE}
htmltools::img(src = "https://sjmgarnier.github.io/viridis/reference/figures/logo.png",
               style = "height:150px;position: absolute;bottom: 3em;left: 7em;")
```

.pull-left[

- 5 different scales
- Also for discrete variables
- [viridis](https://sjmgarnier.github.io/viridis/articles/intro-to-viridis.html) is colour blind friendly and nice in b&w
- In `ggplot2` since v3.0 but no the default
![](https://sjmgarnier.github.io/viridis/articles/intro-to-viridis_files/figure-html/show_scales-1.png)

]

--

.pull-right[

```{r, fig.height=4}
penguins |>
  ggplot(aes(x = bill_length_mm, y = bill_depth_mm, 
             colour = species)) +
  geom_point(alpha = 0.6, size = 5) +
  scale_color_viridis_d()
```
]


---

# Binning instead of gradient

.pull-left[
### Binning help grouping observations
.large[
- Default blue gradient, `viridis` option
- Number of bins, limits can be changed
]
]
```{r binned, eval=FALSE}
penguins |>
  ggplot(aes(x = bill_length_mm, y = bill_depth_mm, 
             colour = body_mass_g)) +
  geom_point(alpha = 0.6, size = 5) +
  scale_color_binned(type = "viridis")
```

.pull-right[
```{r, ref.label='binned', echo=FALSE, fig.height=5}
```
]


---

# Merge similar guides (new v3.3.0)

.pull-left[
#### Size & colour on same variable ➡️ 2 guides
```{r, fig.height=4}
penguins |>
  ggplot(aes(x = bill_length_mm, y = bill_depth_mm, 
             colour = body_mass_g,
             size = body_mass_g)) +
  geom_point(alpha = 0.6) +
  scale_color_viridis_c()
```
]

.pull-right[
.large[`guides()` merge both aesthetics]
```{r, fig.height=4}
penguins |>
  ggplot(aes(x = bill_length_mm, y = bill_depth_mm, 
             colour = body_mass_g, size = body_mass_g)) +
  geom_point(alpha = 0.6) +
  scale_color_viridis_c() +
  guides(colour = "legend") #<<
```

]

---
class: inverse, center, middle


# Facets

```{r, echo=FALSE, fig.height=4,fig.width=6}
ggplot(penguins, aes(bill_depth_mm, bill_length_mm)) +
  facet_grid(species ~ island)
```

---

# Facets: `facet_wrap()`

.pull-left[
### Creating facets
.large[
- Easiest way: `facet_wrap()`
- Use a formula (in R .Large[`~`])
- `facet_wrap()` is for one var
- Or the `vars()` function
]
```{r facet_1, eval = FALSE}
ggplot(penguins, 
       aes(bill_depth_mm, 
           bill_length_mm)) +
  geom_point() +
  facet_wrap(~ species)
```

]

.pull-right[
```{r ref.label='facet_1', fig.height = 5, echo = FALSE}
```
]

---

# Facets layout


.pull-left[
### Specify the number of rows/columns:
.large[
- `ncol = integer` 
- `nrow = integer`
]

```{r facet_2, eval = FALSE}
fc <- ggplot(penguins, 
       aes(bill_depth_mm, 
           bill_length_mm)) +
  geom_point()
fc + facet_wrap(~ species, ncol = 2)
```
]


.pull-right[
```{r facet_2, fig.height = 5, echo = FALSE}
```
]

---

# Facets, free scales

### ⚠️ Make comparison harder

.pull-left[
.large[
- `x` or `y` (`free_x` / `free_y`)
]
```{r, fig.height = 4}
fc + facet_wrap(~ species, scales = "free_y")
```
]

--

.pull-right[
.large[
- Both axis
]
```{r, fig.height = 4}
fc + facet_wrap(~ species, scales = "free")
```
]

---

# `facet_grid()` to lay out panels in a grid


.pull-left[
### Specify a 2 sides **formula** 
.bold[rows] on the left, .bold[columns] on the right separated by a tilde .Large[`~`]

```{r fgrid, eval=FALSE}
ggplot(penguins, 
       aes(bill_depth_mm, 
           bill_length_mm)) +
  geom_point() +
  facet_grid(island ~ species)
```
]

.pull-right[
```{r, ref.label='fgrid', echo = FALSE, fig.height=5}
```
]

---
class: hide_logo

# Barplots in facets

`facet_grid()` can also be used with .bold[one] variable, complemented by a placeholder: .huge[.]

.pull-left[
### Waste of space
```{r, fig.height = 4}
ggplot(starwars) +
  geom_bar(aes(y = gender, fill = sex)) +
  facet_grid(fct_lump_min(species, 4) ~ .) +
  labs(y = NULL)
```
]

--

.pull-right[
### Optimize by removing empty slots

```{r, fig.height = 4}
ggplot(starwars) +
  geom_bar(aes(y = gender, fill = sex)) +
  facet_grid(fct_lump_min(species, 4) ~ .,
             space = "free", scales = "free_y") +
  labs(y = NULL)
```
]

---

# Extensions


`ggplot2` introduced the possibility for the community to [create **extensions**](http://ggplot2.tidyverse.org/articles/extending-ggplot2.html), they are referenced on a [dedicated site](https://exts.ggplot2.tidyverse.org/)


.center[[![](img/plot_extensions.png)](https://exts.ggplot2.tidyverse.org/gallery/)]


---
class: hide_logo

# Fancy nested facets using [`ggh4x`](https://github.com/teunbrand/ggh4x), code by Eric Koncina


```{r, echo = FALSE}
htmltools::img(src = "https://teunbrand.github.io/ggh4x/logo.png",
               style = "height:150px;position: absolute;top: 1em;right: 1em;")
```

.pull-left[
```{r, facet_nested, eval = FALSE}
library(ggh4x)
library(ggtext)

strip_ektheme <- strip_nested(
  text_x = distribute_args(
    size = c(14, 10), colour = c("darkred", "black"), 
    face = c("bold", "plain"),
    fill = list(NA, "lightblue"),
    box.color = list(NA, "darkblue"),
    halign = list(0.5, 0.5), valign = list(1, 0.5),
    linetype = list(0, 1), r = list(NULL, unit(5, "pt")),
    width = list(NULL, unit(1, "npc")),
    padding = list(NULL, margin(5, 0, 4, 0)),
    margin = list(margin(1, 0, 2, 0), margin(7, 3, 3, 3)),
    .fun = element_textbox),
  size = "variable",
  by_layer_x = TRUE
)
pivot_longer(penguins, names_to = c("what", "measure"),
             values_to = "value", 
             bill_length_mm:body_mass_g, names_sep = "_") |> 
  ggplot(aes(x = species, y = log(value))) +
  geom_violin(trim = FALSE) +
  facet_nested(~ what + measure, strip = strip_ektheme, #<<
               nest_line = TRUE, resect = unit(15, "mm"), #<<
               axes = TRUE, remove_labels = TRUE) + #<<
  theme_classic(14) +
  theme(strip.background = element_blank(), 
        axis.text.x = element_text(angle = 45, hjust = 1),
        ggh4x.facet.nestline = element_line(colour = "rosybrown")) #<<
```
]

--

.pull-right[
</br>
</br>
```{r, ref.label="facet_nested", echo=FALSE, fig.height=7, fig.width=8, warning=FALSE}
```
]

---
class: inverse, center, middle


# Cosmetic 💅 

### and helpers


---

# Black theme

.pull-left[
### Using Bob Rudis [`hrbrthemes`](https://github.com/hrbrmstr/hrbrthemes) package
```{r ipsum_ft, eval=FALSE}
library(hrbrthemes)
ggplot(penguins,
       aes(x = bill_length_mm,
           y = bill_depth_mm,
           colour = species)) +
  geom_point() +
  geom_smooth(method = "lm", formula = 'y ~ x',
      # no standard error ribbon
              se = FALSE) +
  facet_grid(island ~ .) +
  labs(x = "length (mm)", y = "depth (mm)",
       title = "Palmer penguins",
       subtitle = "bill dimensions over location and species",
       caption = "source: Horst AM, Hill AP, Gorman KB (2020)") +
  # hrbrthemes specifications
  scale_fill_ipsum() +
  theme_ft_rc(14) +
  # tweak the theme
  theme(panel.grid.major.y = element_blank(),
   panel.grid.major.x = element_line(size = 0.5),
   plot.caption = element_text(face = "italic"),
   strip.text = element_text(face = "bold"),
   plot.caption.position = "plot")
```
]

--

.pull-right[
```{r, out.width="100%", echo = FALSE}
knitr::include_graphics("img/plot_theme_fc.png")
```
]



---

# Compose plots with `patchwork`

```{r, echo = FALSE}
htmltools::img(src = "https://raw.githubusercontent.com/thomasp85/patchwork/master/man/figures/logo.png",
               style = "height:120px;position: absolute;top: 1em;right: 1em;")
```

#### [Patchwork](https://patchwork.data-imaginist.com/) is developed by [Thomas Lin Pedersen](https://github.com/thomasp85), main maintainer of `ggplot2`.

.pull-left[
.large[Define 3 plots and assign them names]
```{r, fig.height = 1.5, fig.width= 3.5}
p1 <- ggplot(penguins, 
             aes(x = species)) +
  geom_bar() +
  labs(title = "Species distribution")
p2 <- ggplot(penguins, 
             aes(y = island)) +
  geom_bar() +
  labs(title = "Island but flipped")
p3 <- ggplot(penguins, 
             aes(x = body_mass_g,
                 y = bill_depth_mm,
                 colour = sex)) +
  geom_point()
p1
```

]

.pull-right[
```{r, fig.height = 1.5, fig.width=3.5}
p2
p3
```

.Large[Now, compose them!]
]



---
class: hide_logo

# Compose plots with `patchwork`

```{r, echo = FALSE}
htmltools::img(src = "https://raw.githubusercontent.com/thomasp85/patchwork/master/man/figures/logo.png",
               style = "height:120px;position: absolute;top: 1em;right: 1em;")
```

### `patchwork` provides an API using the classic arithmetic operators

.pull-left[
```{r patchwork2, eval = FALSE}
library(patchwork)
(( p1 | p2 ) / p3) +
  # add tags and main title
  plot_annotation(tag_levels = 'A',
                  title = 'Plots about penguins') &
  # modify all plots recursively
  theme_minimal() +
  theme(text = element_text('Roboto'))
```

]

.pull-right[
```{r, ref.label="patchwork2", echo = FALSE, fig.width=7}
```
]

.footnote[If you need to perfectly align axes, have a look at [`cowplot`](https://wilkelab.org/cowplot/) by .bold[Claus O. Wilke]]


---
class: hide_logo

# Polished example by Cédric Scherer

```{r, echo = FALSE}
htmltools::img(src = "https://github.com/Z3tt.png",
               style = "height:120px;position: absolute;top: 1em;right: 1em;")
```

```{r, echo = FALSE}
htmltools::img(src = "https://github.com/thomasp85.png",
               style = "height:120px;position: absolute;top: 8em;right: 1em;")
```

```{r, echo = FALSE}
htmltools::img(src = "https://github.com/clauswilke.png",
               style = "height:120px;position: absolute;top: 15em;right: 1em;")
```

.Large[Using [`patchwork`](https://www.data-imaginist.com/2020/insetting-a-new-patchwork-version/),  [`ggforce`](https://github.com/thomasp85/ggforce)] (.bold[T. Pedersen]) and [`ggtext`](https://wilkelab.org/ggtext/) (.bold[Claus O. Wilke])

.center[<img src = "img/plot_scherer_penguins.png", style = "height:450px;"/>]


.footnote[Source: [OutlierConf](https://www.youtube.com/watch?v=7UjA_5gNvdw&list=PLAm5TIX-yz7IkKOUcStM_vl8AD0S9v0co&index=34) by [Cédric Scherer](https://github.com/Z3tt), see also [code](https://github.com/Z3tt/OutlierConf2021/blob/main/R/OutlierConf2021_ggplotWizardry_HandsOn.Rmd)]


---

```{r scherer, include = FALSE, fig.height=7, fig.width=10}
culmen <- png::readPNG("img/plot_culmen_depth.png", native = TRUE)
penguins |> 
  filter(across(ends_with("mm"), \(x) !is.na(x))) |> 
  ggplot(aes(x = bill_length_mm, y = bill_depth_mm)) +
  ggforce::geom_mark_ellipse(
    aes(fill = species, label = species), alpha = 0,
    show.legend = FALSE
  ) +
  geom_point(aes(color = body_mass_g), alpha = .6, size = 3.5) + 
  scale_x_continuous(breaks = seq(25, 65, by = 5), limits = c(25, 65)) +
  scale_y_continuous(breaks = seq(12, 24, by = 2), limits = c(12, 24)) +
  scale_color_viridis_c(option = "turbo", labels = scales::comma) +
  labs(
    title = "Bill Dimensions of Brush-Tailed Penguins (<i style='color:#28A87D;'>*Pygoscelis*</i>)",
    subtitle = 'A scatter plot of bill depth *versus* bill length.',
    caption = "Data: Gorman, Williams & Fraser (2014) *PLoS ONE*",
    x = "Bill Length (mm)", 
    y = "Bill Depth (mm)",
    color = "Body mass (g)") +
  theme_minimal(base_family = "RobotoCondensed", base_size = 12) +
  theme(plot.title = element_markdown(face = "bold"),
        plot.subtitle = element_markdown(),
        plot.caption = element_markdown(margin = margin(t = 15)),
        axis.title.x = element_markdown(),
        axis.title.y = element_markdown()) +
  theme(plot.title.position = "plot") +
  theme(plot.caption.position = "plot") +
  theme(legend.position = "top") +
  guides(color = guide_colorbar(title.position = "top", 
                                title.hjust = .5, 
                                barwidth = unit(20, "lines"), 
                                barheight = unit(.5, "lines"))) +
  coord_cartesian(expand = FALSE, clip = "off") +
  theme(plot.margin = margin(t = 25, r = 25, b = 10, l = 25)) +
  labs(caption = "Data: Gorman, Williams & Fraser (2014) *PLoS ONE* &bull; Illustration: Allison Horst") +
  patchwork::inset_element(culmen, left = 0.82, bottom = 0.83, right = 1, top = 1, align_to = 'full')
```

`r chunk_reveal("scherer")`

```{r, eval=FALSE, echo=FALSE}
# for saving the plot above
ggsave("scherer_penguins.png",device = ragg::agg_png, 
       res = 160, height = 1000, width = 1400, units = "in", 
       limitsize = FALSE)
```


---
class: hide_logo

# Text paths, curved labels in data

```{r, echo = FALSE}
htmltools::img(src = "https://allancameron.github.io/geomtextpath/logo.png",
               style = "height:150px;position: absolute;bottom: 1em;right: 15em;")
```

.pull-left[
```{r, warning=FALSE, fig.height=4.8}
library(geomtextpath)
ggplot(penguins,
       aes(x = bill_length_mm, y = bill_depth_mm, colour = species)) +
  geom_point(alpha = 0.3) +
  stat_ellipse(aes(label = species),
    geom = "textpath", hjust = c(0.55)) +
  theme_bw(14) + theme(legend.position = "none")
```

]

.pull-right[
```{r, warning=FALSE, fig.height=4}
ggplot(penguins,
       aes(x = bill_length_mm / bill_depth_mm, 
           colour = species)) +
  stat_density(aes(label = species),
    geom = "textpath", hjust = c(0.25)) +
  theme_bw(14) + theme(legend.position = "none")
```

]

.footnote[**Allan Cameron**: [geomtextpath](https://allancameron.github.io/geomtextpath) package]


---
class: hide_logo

# Exporting, interactive or passive mode

.pull-left[
### Right panel 
- Using the Export button in the _Plots_ panel

![](img/plot_export_plot.png)
### ggsave 

- Save the `ggplot` **object**, 2nd argument, here `p` or `last_plot()`
- Guesses the output type by the extension (`jpg`, `png`, `pdf` _etc._)

```{r eval = FALSE}
ggsave("my_name.png", p, width = 60, height = 30, units = "mm")
ggsave("my_name.pdf", last_plot(), width = 50, height = 50, units = "mm")
```
]

.pull-right[
### Rmarkdown docs

- If needed, adjust the chunk options:
  + Size: `fig.height`, `fig.width`
  + Ratio: `fig.asp`...
  + [Others](https://yihui.name/knitr/options/?version=1.1.214&mode=desktop)

![](img/plot_chunk_options.png)
]

---
class: hide_logo

# Plot your data!

.flex[
.w-60.bg-washed-yellow.b--red.ba.bw2.br3.shadow-5.ph2.mt0.ml6[
.Large[

> Never trust summary statistics alone; always visualize your data

.tr[
— .bold[_Alberto Cairo_]]
]
]
]

.center[
![](https://d2f99xq7vri1nk.cloudfront.net/DinoSequentialSmaller.gif)]

.footnote[source: Justin Matejka, George Fitzmaurice [Same Stats, Different Graphs...](https://www.autodeskresearch.com/publications/samestats)]

---

# Missing features

.pull-left[

### Geoms list [here](http://ggplot2.tidyverse.org/reference/index.html#section-layer-geoms) 

- `geom_tile()` heatmap
- `geom_bind2d()` 2D binning
- `geom_abline()` slope

### Stats list [here](http://ggplot2.tidyverse.org/reference/index.html#section-layer-stats)


- `stat_ellipse()` (`ggforge` seen)
- `stat_summary()` easy mean, 95CI etc.

### Plot on multi-pages 

- `ggforce::facet_grid_paginate()` facets
- `gridExtra::marrangeGrob()` plots
]

.pull-right[

### Coordinate / transform 

- `coord_cartesian()` for zooming in

### Customise [_theme_](http://ggplot2.tidyverse.org/reference/index.html#section-themes) elements 

- Legend & guide tweaks
- Major/minor grids
- Font, faces
- Margins
- Labels & ticks
- Strip positions
- See [live examples](https://www.r-graph-gallery.com/ggplot2-package.html) of pre-built themes

]

---

# Build your plot with point and click

### Using `esquisse` Rstudio addin by [dreamRs](https://github.com/dreamRs/esquisse)

```{r, echo = FALSE}
htmltools::img(src = "https://raw.githubusercontent.com/dreamRs/esquisse/master/man/figures/logo.png",
               style = "height:150px;position: absolute;top: 1em;right: 1em;")
```


.center[<img src = "https://raw.githubusercontent.com/dreamRs/esquisse/master/man/figures/esquisse.gif", style = "height:450px">]


---
class: hide_logo

# Before we stop

.flex[
.w-50.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt2.ml1[
.large[.gbox[You learned to:]

- Apprehend facets
- Color schemes and gradients
- Discover extensions
- The endless potential of cosmetic improvements
]]
.w-50.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt2.ml2[
.large[.bbox[Acknowledgments  🙏 👏]]

* [Thomas Lin Pedersen](https://github.com/thomasp85), great webinar _Plotting everything with ggplot2_:
  + [part 1, ggplot2 API](https://www.youtube.com/watch?v=h29g21z0a68)
  + [part 2, extending ggplot2](https://www.youtube.com/watch?v=0m4yywqNPVY)
* [Bob Rudis](https://rud.is/)
* [Allison M. Horst, Alison P. Hill and Kristen B. Gorman](https://github.com/allisonhorst/palmerpenguins) `*`
* [DreamRs](https://www.dreamrs.fr/) (Victor Perrier, Fanny Meyer)
* [Claus O. Wilke](https://clauswilke.com/)
* [Hadley Wickham](https://github.com/hadley)
* [Cédric Scherer](https://cedricscherer.netlify.app)
]]
.flex[
.w-50.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt2.ml1[
.large[.ybox[Further reading 📚]]

- [ggplot2 book](https://ggplot2-book.org/) by H. Wickham (free access)
- [**NEW** official FAQ](https://ggplot2.tidyverse.org/articles/articles/faq-axes.html), axes, facets, custom, annot, reorder, bars 
- [R for Data Science](https://r4ds.had.co.nz/) by H. Wickham / G. Gromelund (free access)
- [TidyTuesday challenges](https://www.tidytuesday.com/)
- [Vizualisation gallery](https://cedricscherer.netlify.app/top/dataviz/) by Cédric Scherer
- [plotting tutorial](https://cedricscherer.netlify.app/2019/08/05/a-ggplot2-tutorial-for-beautiful-plotting-in-r) by Cédric Scherer
]
.w-50.pv2.ph3.mt2.ml1[
.huge[.bbox[Thank you for your attention!]]
]]

.center[.footnote[`*`: Palmer penguins, data are available by CC-0 license and `Artwork` by Allison Horst]]
