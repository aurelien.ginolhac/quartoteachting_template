---
title: "Import data"
subtitle: "`r params$subtitle`"
author: "`r params$author`"
date: "`r params$date`"
params:
  title: "Data import"
  subtitle: "From flat files and excel files"
  author: "Roland Krause"
  date: "7 February 2023"
output:
  xaringan::moon_reader:
    nature:
      highlightLines: true
---
class: title-slide


```{r setup-lecture, child = '_setup_lecture.Rmd'}
```

```{r libs, include=FALSE}
knitr::opts_chunk$set(warning = FALSE, comment = '')
library(xaringan)
library(xaringanthemer)# gadenbuie/xaringanthemer
library(xaringanExtra) # gadenbuie/xaringanExtra
library(countdown)
library(tidyverse)
```



# `r params$title`

## `r params$subtitle`

.center[<img src="img/00/logo_readr.png" width="100px"/>
        <img src="img/00/logo_readxl.png" width="100px"/>]

### `r params$author` | `r course_name` | `r params$date`



---

# Learning objectives

.flex[
.w-70.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt3.mr1.ml6[
.large[.gbox[You will learn to:]
.float-img[`r fontawesome::fa("book-reader", height="50px")` ] 

- Download a data file to a dedicated subfolder in your project
- Learn about `tibbles` 
- Use `readr` to import flat-files data into _R_
- Use the interactive RStudio interface to visualise your data import
- Appreciate tibbles
- Use `readxl` to import excel files into _R_

]
]
]

---

# Importing data

.pull-left[

.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.pb4.mt3.mr1[
.large[.gbox[Getting started]


 - Represents the first step of your work

 - .bold[R base] already provides functions for text files
  +  `read.csv()`, `read.delim()`, ...
  + So why redefine?

 - But what about

    + Excel files (`.xls`, `.xlsx`)
    + Foreign statistical formats 
      +   `.sas` from SAS
      +   `.sav` from SPSS
      +   `.dta` from Stata
    + databases (SQL, SQLite ...)


]
]
]



--

.pull-right[

.bg-washed-yellow.b--gold.ba.bw2.br3.shadow-5.ph3.mt3.mr1.pb3[
.large[.bbox[Tidyverse implementation]

```{r, echo = FALSE}
htmltools::img(src = "img/00/logo_tidyverse.png",
               style = "float:right;height:100px;")
```


  + More **speed**
  + improved handling of column types generates more consistent data structures
  + generates `tibbles`


.bbox[Tibbles]

```{r, echo = FALSE}
htmltools::img(src = "img/00/logo_tibble.png",
               style = "float:right;height:100px;")
```

- Have a refined print method that shows only the first 10 rows.
- Show all the columns that fit on screen and list the name of remaining ones.
- Each column reports its type (`double, int, lgl, chr`)
- Makes it much easier to work with large data.



]
]
]

---
class: hide_logo

# `data.frame` vs `tibble`

.pull-left[

### Data frame

```{r}
swiss
```
]

--

.pull-right[
### Tibble


```{r out.width=7}
as_tibble(swiss)
```

]

---

# Rownames in tibbles


.left-column[
.bg-washed-yellow.b--gold.ba.bw2.br3.shadow-5.ph3[
.bbox[Rownames `r fontawesome::fa("lightbulb")`]

Base data frames have rownames, which a special type of column that need special treatments.

]
]
.right-column[

`tibbles` never use `rownames`!  

When converting regular a `data.frame` they are lost unless you assign them to a dedicated column.

```{r}
# library(tibble)
as_tibble(swiss, rownames = "Province")
```

Importing data using `readr` function such as `read_csv()` will not create them.

]


---

# The tidyverse packages to import your data

.flex[
.w-70.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt2.mr1[
.large[.ybox[`readr`]]
* `read_csv()`: comma separated (`,`)
* `read_csv2()`:  separated (`;`)
* `read_tsv()`: tab separated
* `read_delim()`: general delimited files,  auto-guesses delimiter
* `read_fwf()`: fixed width files
* `read_table()`: columns separated by white-space(s)
.right[![](img/00/logo_readr.png)]

]
.w-70.bg-washed-yellow.b--green.ba.bw2.br3.shadow-5.ph3.mt2.mr1[
.large[.ybox[`readxl`]]

* `read_excel()`
* `read_xls()`
* `read_xlsx()`
.right[![](img/00/logo_readxl.png)]

]
.w-70.bg-washed-blue.b--red.ba.bw2.br3.shadow-5.ph3.mt2.mr1[
.large[.rbox[haven]]


- `read_sas()` for SAS
- `read_sav()` for SPSS
- `read_dta()` for Stata
.right[![](img/00/logo_haven.png)]

]
]

---

class: inverse, middle, center
# Importing flat files

<img src="img/00/logo_readr.png" width="150px"/>

---
class: slide-practical

# Follow along: Downloading files

.w-90.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt3.ml5[
.large[.gbox[Flat file example: _swiss.csv_]]

- Use the new RStudio project (finding your files will be easier)
- Download the [`swiss.csv`](https://biostat2.uni.lu/practicals/data/swiss.csv) file to your project folder, in the sub-folder `data`
- Open the file with a text viewer and have a look at its content
- Does the delimiter fit the file extension?

```{r}
download.file("https://biostat2.uni.lu/practicals/data/swiss.csv", "data/swiss.csv")
readr::read_delim("data/swiss.csv")
```
]

---
class: slide-practical

# Manual download (alternatively)

.pull-left.large[
In your browser:

- Right-click on the .bold[link]
- Choose .bold[Save Link As]

![](img/import_save_as.png)


]


--
.pull-right[
.large[
- You .red.bold[MUST] have created a RStudio project
- Use Finder/Explorer to select this project folder
- If not present .bold[New Folder] called `data`
- Go inside this sub-folder `data`
- Hit .bold[Save]
]

![](img/import_in_data.png)
]

???
It's always useful to check in your file browser if files that you downloaded
programmatically are actually where you think they are.

---

# The [readr 2.0](https://readr.tidyverse.org/)  engine is fast

```{r, echo = FALSE}
htmltools::img(src = "img/00/logo_readr.png",
               style = "height:90px;position: absolute;top: 1em;right: 1em;")
```

.pull-left[
### `readr` is the tidyverse import package 
 
* Column type guessing
* Reporting parsing problems
* Progress bar
* Reading from URLs
* Reading compressed files


#### Great features in `readr` with version 2.0

[Jim Hester](https://github.com/jimhester) developed `vroom`.
All of its functionality are included in the current `readr` (v2.1.2)
No need to use `vroom`.

  + Delimiter guessing
  + **FAST!**
      + multi-threaded
      + use [ALTREP](https://svn.r-project.org/R/branches/ALTREP/ALTREP.html)
      + Sadly disabled by default due to Windows issues
  + Column selection
  + Merge multiple files into one `tibble`
]

--

.pull-right[


![](https://vroom.r-lib.org/articles/benchmarks_files/figure-html/unnamed-chunk-2-1.png)

.footnote[source: `vroom` [benchmarks](https://vroom.r-lib.org/articles/benchmarks.html)]
 
]


---
class: hide_logo

# Catch and report reading issues

.pull-left[
### Toy example with two issues
```{r}
read_delim("data/swiss_ray.csv")
```

- two columns with missing data
- `fer` is no longer of type `double`
- empty `ray` column imported as `logical`!

]

--
.pull-right[
### Specifying column types with `cols()`

`readr` records issues. Use `problems()` to see them

```{r, paged.print=FALSE}
sw2 <- readr::read_delim("data/swiss2.csv",
             col_types = cols(fer = col_double(),
                              agri = col_double(),
                              exa = col_integer(),
                              edu = col_integer(),
                              cath = col_double()))

sw2
```



```{r}
problems(sw2)
```

]

---

# Choosing which columns to read

.pull-left[
### Lighter column type definitions

here, we want to **skip** the wrong columns: `fer` and `cath`.

We let `read_delim` guessing the `agri` type (`?`), see all correspondences [here](https://vroom.r-lib.org/articles/vroom.html#column-types). 


```{r}
readr::read_delim("data/swiss2.csv",
      col_types = cols(fer="_", 
                       agri = "?",
                       exa = "i",
                       edu = "i",
                       cath = "_",
                       ray = "_"))
```
]

--

.pull-right[

### Columns selection 

#### Supported by `tidyselect` syntax.

```{r}
readr::read_delim("data/swiss_ray.csv",
      col_select = c(agri, edu))
```

]

---
class: hide_logo

# Column selection with [`tidyselect`](https://tidyselect.r-lib.org/)

.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt1.mb2.large[
This package is the backend for programatic column selection in the `tidyverse`.
Features are described in [this article](https://tidyselect.r-lib.org/reference/language.html).
]

--

.pull-left[
.bg-washed-yellow.b--gold.ba.bw2.br3.shadow-5.ph3.mr1[
.bbox[Operators]
- `[c()]` for .bold[combining] bare names
- `[:]` for selecting a .bold[range]
- `[-]` for .bold[negating] a selection

.bbox[Helpers]
- `everything()` .bold[all] columns
- `last_col()` .bold[last] column

.bbox[Using patterns]
- `starts_with()` with quoted prefix
- `ends_with()` with quoted suffix
- `contains()` with quoted string

]
]
--

.pull-right[


```{r}
readr::read_delim("data/swiss2.csv", show_col_types = FALSE,
      col_select = starts_with("e"), n_max = 1)
```


```{r}
readr::read_delim("data/swiss2.csv", show_col_types = FALSE,
      col_select = contains("e"), n_max = 1)
```

```{r}
readr::read_delim("data/swiss2.csv", show_col_types = FALSE,
      col_select = fer:edu, n_max = 1)
```


]

---

# Reading data with no column header

.pull-left[

### Example

- Using the URL https://basv53.uni.lu/lectures/data/example.csv
- This toy data contains 3 columns


.arge[Content is:]

```
dog,red,1
cat,blue,2
chicken,green,6
```

]

--

.pull-right[
### Naive approach

```{r, eval = TRUE}
exa <- "https://basv53.uni.lu/lectures/data/example.csv"
readr::read_delim(exa)
```

.center[.bold.large[Satisfying?]]

]

---

# Provide the header

.pull-left[
### Read all lines

- Colnames are self-created.
- `show_col_types = FALSE` suppresses the verbose mode

```{r, paged.print=FALSE}
read_delim(exa, col_names = FALSE, show_col_types = FALSE)
```

.center[.bold.large[Satisfying?]]

]

--

.pull-right[
### Supply the colnames

- Remember we are still reading directly from the url in the `exa` variable
```{r}
read_delim(exa, col_names = c("animal", "color", "value"),
           show_col_types = FALSE)
```

.center[.bold.large[Better]]

]


---

class: hide_logo

# Reference for column types

.large[
Column types are specified using `col_types = cols()`

Function | Short | Description
----|-----|-----
 `col_logical()`  | .large.bold[l] | TRUE/FALSE, 1/0
 `col_integer()` | .large.bold[i] | integers
 `col_double()` | .large.bold[d] | floating point values.
 `col_number()` | .large.bold[n] | numbers containing the grouping_mark
 `col_date(format = "")`  | .large.bold[D] | with the locale's date_format
 `col_time(format = "")`| .large.bold[t] | with the locale's time_format
 `col_datetime(format = "")`| .large.bold[T] |  ISO8601 date times
 `col_factor(levels, ordered)`  |.large.bold[f] |  a fixed set of values
 `col_character()`|  .large.bold[c] |everything else
 `col_skip()` |.large.bold[_] | .large.bold[-], don't import this column
 `col_guess()` | .large.bold[?] | parse using the “best” type based on the input
]


---

## Examples for data-aware inputs

.pull-left[
### Using one-letter shortcuts
```{r}
readr::read_delim(exa, 
      col_names = c("animal", "color", "value"),
      col_types = cols(
        animal = "c",
        color = "_",
        value = "i"
      ))
```
]

--

.pull-right[
### Even more compact
```{r}
readr::read_delim(exa, 
      col_names = c("animal", "color", "value"),
      col_types = "c_i")
```

- Use the single character code in the column order
]

---

# Skipping lines 


.flex[
.w-70.bg-washed-blue.b--blue.ba.bw2.br3.shadow-5.ph3.mt3.mr1[

.bbox[Our example]
```{r, include=FALSE}
 disp_exa <-   readr::read_delim(exa,
                                 col_names = c("animal", "color", "value"),
      col_types = "c_i")
```

```{r, echo = FALSE}
disp_exa
```

]


.w-70.bg-washed-yellow.b--gold.ba.bw2.br3.shadow-5.ph3.mt3.mr1[

.bbox[Comment]
Do not read lines beginning with a character.
```{r}
readr::read_delim(
  exa,
  comment = "d", #<<
  col_names = FALSE,
  show_col_types = FALSE)
```


]

.w-70.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt3.mr1[
.bbox[Skip lines]
```{r}
readr::read_delim(
  exa,
  skip = 1, #<<
  col_names = FALSE,
  show_col_types = FALSE)
```
]

.w-70.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt3.mr1[

.bbox[Limited number of lines]
```{r}
readr::read_delim(
  exa,
  skip = 1,
  n_max = 1, #<<
  col_names = FALSE,
  show_col_types = FALSE
)
```

]
]


---

# Several files at once

.pull-left[
### Multiple files are read into one tibble
```{r}
readr::read_csv(c("data/swiss.csv",  "data/swiss.csv"))
```
]

.pull-right[

### Files with different columns are not read

```{r, error = TRUE}

readr::read_csv(c("data/swiss.csv", "data/swiss2.csv"))
```



.bg-washed-yellow.b--gold.ba.bw2.br3.shadow-5.ph3.ma1[
 Pass the input column names if not in agreement.
 File names are recorded by providing the `id` argument.

]

```{r}

readr::read_csv(c("data/swiss_ray.csv", "data/swiss.csv"),
                 col_name = c("fer", "agri", "exa", "edu",
                              "cath", "inf", "ray"),
                id =  "orig",
                show_col_types = FALSE)
                       
                  
```
]

Can you spot the error?

---

```{r, echo = FALSE}
htmltools::img(src = "img/00/logo_readxl.png",
               style = "height:90px;position: absolute;top: 1em;right: 1em;")
```

## Reading Excel files

#### **readxl** package is part of tidyverse and makes it easy to import tabular data from Excel spreadsheets with several options. 

More details are described in this [pkgdown website](https://readxl.tidyverse.org/)

.pull-left[
### Features
- `readxl` author: [Hadley Wickham](https://github.com/hadley) and [Jenny Bryan](https://github.com/jennybc)
- `read_excel()` reads both `xls` and `xlsx` files and detects the format from the extension. Otherwise:
    + `read_xls()`
    + `read_xlsx()`
- Return tibbles
- Column type guessing
- Discovers the minimal data rectangle and returns that, by default
- No external library dependencies, _e.g._, Java or Perl

]

.pull-right[
### Usage guidance
- Exert more control with `range`, `skip`, and `n_max`
- `excel_sheets()` returns the sheet names
- Combine multiple sheets, jointly with `purrr::map_df()`
- `col_types` arguments match Excel's data types `r fontawesome::fa("triangle-exclamation")`
  - "numeric", "text", "skip", "guess", 


]

---

# Additions to `readr` and `readxl`

.pull-left[

.bg-washed-yellow.b--yellow.ba.bw2.br3.shadow-5.ph3.mt1.mr1[
.large.left[.bbox[Need for (more) speed ]

Check `fread()` from [`data.table`](https://github.com/Rdatatable/data.table/wiki) 

```{r, echo = FALSE}
htmltools::img(src = "https://raw.githubusercontent.com/wiki/Rdatatable/data.table/icons/sticker.png",
               style = "float:right;height:100px;")
```

- Stable 
- No dependencies
- Overhauled
- Could feed from `bash` command
- Could be faster than `readr` functions
- Only relevant if you have data sets in the Gbyte range to read
]
]

]

.pull-right[
.bg-washed-blue.b--blue.ba.bw2.br3.shadow-5.ph3.mt1.mr1[
.large[

.gbox[Import Google spreadsheets ]

```{r, echo = FALSE}
htmltools::img(src = "https://raw.githubusercontent.com/tidyverse/googlesheets4/main/man/figures/logo.png",
               style = "float:right;height:100px;")
```
* Google spreadsheet can be read with `googlesheets4` package
* Well developed package


.rbox[Remember haven]
```{r, echo = FALSE}
htmltools::img(src = "img/00/logo_haven.png",
               style = "float:right;height:100px;")
```

* Most foreign formats can be converted with the `haven` package.
* Save the €.


]

]
]


---

# Good to know `r fontawesome::fa("lightbulb", fill = "orange")`

.pull-left[

### Hell are other people's column names

.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt1.mr1[

.bbox[Fixing column names on import ]

 Duplicated, Missing, Ugly (e.g. `-` , `%`)
 
Colnames are repaired with several options for their treatment (`"minimal", "unique", "universal", "check_unique"`). 

See [details](https://vctrs.r-lib.org/reference/vec_as_names.html) in the `vctrs` package description.



]
]

--

.pull-right[
```{r}
readxl::read_excel("data/bad-table-psych.xlsx", 
                         skip = 2,
                         n_max = 12,
                   .name_repair = "universal") |> 
  rename_with(\(x) str_to_lower(x))  |> 
  rename_with(\(x) str_replace_all(x, "\\.+", "_"))
```

]


---

## Best practices `r fontawesome::fa("anchor")`
.pull-left[
### General advice 
.large[
* Structure your input data process.
* Load all data early in your process and in a central place unless there are specific performance issues.
* Refactor your inputs, don't rely on changing things while analysing data.
* Do not read the whole file just because you can for your production code. 
* Avoid downloading manually - use API calls or download data programmatically

]
]

.pull-right[
### Tidy imports
.large[
* Check the [cheat sheet](https://github.com/rstudio/cheatsheets/blob/main/data-import.pdf)
* Read columns you want explicitly with appropriate `col_types()`
* You `tidyselect` helpers to specify many similar columns
* Clean up column names with `dplyr` and `stringr` functions early.
 
]

]


---

class: hide_logo

# Before we stop

.flex[
.w-50.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt2.ml1[
.large[.gbox[You learned to:]

- Appreciate the tibble features
- Learn set-up working directory with RStudio `projects`
- Learn the `tidyselect` syntax
- Use `readr::read_delim` to import your flat files
- Adjust the imported data types
- Use `readxl` to import excel files



.gbox[Further reading]

- Import in [R for Data Science](http://r4ds.had.co.nz/data-import.html)

]

]

.w-50.bg-washed-green.b--green.ba.bw2.br3.shadow-5.ph3.mt2.ml2[
.large[.bbox[Acknowledgments  🙏 👏]

Development
* Eric Koncina (initial installment)
* Veronica Codoni (swiss data set)
* Aurélien Ginolhac (`vroom` development)
* Roland Krause (`readr` 2.0 update)

Input and inspiration
* Jim Hester (`vroom`, `readr` development)
* Jenny Bryan (advice with project organization)
* Hadley Wickham
* Nicholas Tierney
]
]
]
.w-50.pv2.ph3.mt1.ml1[
.huge[.bbox[Thank you for your attention!]]

]


